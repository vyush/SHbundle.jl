using SHbundle
"""
    clm2sc(clm ; max_lm = Inf,gcoef2sc = false)
`CLM2SC` converts a list of coefficients in clm-format to /S|C\\ - format and if available does the same with the standard deviations.
# Input
- `clm::Array`: Coefficients as matrix in the form `[l m C S sigma_C, sigma_S]`.
- `max_lm::Integer`: Only coefficients up to a degree = order = max_lm are used. (`Default:Inf. i.e, read all coefficients`).
- `gcoef2sc::Boolean`:provides functionality of deprecated function gcoef2sc( `Default: False` )
#Output
- `sc::Array`: coefficients in /S|C\\ format.
# Examples
```julia-repl
julia> clm2sc(sh_coeff,max_lm = 2)
3×5 Array{Float64,2}:
  0.0         0.0          1.0           0.0          0.0
  0.0         0.0          0.0           0.0          0.0
 -1.40029e-6  1.41065e-9  -0.000484169  -3.10267e-10  2.43937e-6
```
# Uses
- `Package`: Interpolations,Dierckx.
- `Functions`: sub2indv.
"""
function clm2sc(clm ; max_lm = Inf,gcoef2sc = false)
    t = (clm[:,1] .<= max_lm) .& (clm[:,2] .<= max_lm);
    clm = clm[t,:];

    t = max(findmax(clm[:,1])[1] ,findmax(clm[:,2])[1])
    if(t<max_lm)
        max_lm = floor(Int,t);
    end
    max_lm = floor(Int,max_lm)
    if(!gcoef2sc)
        v1 = zeros(max_lm + 1, 2 * (max_lm + 1) - 1);
    else
        v1 = ones(max_lm + 1, 2 * (max_lm + 1) - 1) * 1e-40;
        v2 = ones(max_lm + 1, max_lm + 1) * 1e-40;
        v3 = ones(max_lm + 1, max_lm + 1) * 1e-40;
    end

    #print(size(v1))
    indices = [(floor(Int64,clm[1, 1] + 1), floor(Int64,max_lm + 1 - clm[1, 2]))]
    indices2 = [(floor(Int64,clm[1, 1] + 1), floor(Int64,max_lm + 1 + clm[1, 2]))]
    for i = 2:size(clm,1)
        indices = [indices (floor(Int64,clm[i, 1] + 1), floor(Int64,max_lm + 1 - clm[i, 2]))]
        indices2 = [indices2 (floor(Int64,clm[i, 1] + 1), floor(Int64,max_lm + 1 + clm[i, 2]))]
     end


      idx_s = sub2indv(size(v1), indices)
      idx_c = sub2indv(size(v1), indices2)
      idx_s = idx_s;
      idx_c = idx_c;
      v1[idx_s] = clm[:,4]
      v1[idx_c] = clm[:,3]

#         v1[floor(Int64,clm[1, 1] + 1),floor(Int64,max_lm + 1 - clm[1, 2])] = clm[1, 4]
#         v1[floor(Int64,clm[1, 1] + 1),floor(Int64,max_lm + 1 + clm[1, 2])] = clm[1, 3]
#         for i = 2:size(clm,1)
#             v1[floor(Int64,clm[i, 1] + 1),floor(Int64,max_lm + 1 - clm[i, 2])] = clm[i, 4]
#             v1[floor(Int64,clm[i, 1] + 1),floor(Int64,max_lm + 1 + clm[i, 2])] = clm[i, 3]
#         end
      return v1;
end
