using SHbundle
using Interpolations,Dierckx
"""
    lovenr(n)
`LOVENR` gives the LOVE number of the elastic earth for a certain degree n.
# Input
- `n::Integer`: spherical harmonic degree (up to 200)
# Output
- `kn:Float`: LOVE number of degree n.
# Examples
```julia-repl
julia> lovenr(7)
-0.081
```
# Uses
- `Packages`:Interpolations,Dierckx
- `Functions`: interpolate, Gridded, Linear;
"""
function lovenr(n)
    x  = [0.0;   1 ;    2  ;   3  ;   4    ; 5 ;   6 ;   7 ;   8   ; 9  ; 10  ; 12  ; 15 ;  20  ; 30   ;40  ; 50  ; 70  ;100 ; 150 ; 200];
    y = [0.0 ;270 ;-3030 ;-1940 ;-1320 ;-1040; -890; -810; -760; -720; -690 ;-640; -580; -510; -400; -330 ;-270 ;-200 ;-140 ;-100 ; -70];
    y = y ./1e4;
    itp = interpolate((x,), y, Gridded(Linear()))
    return itp(n);
end
