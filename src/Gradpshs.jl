"""
    gradpshs(field,lamRAD,phiRAD,r;lmax = Inf,constants = [],drtype = "lscs",jflag = 1)
`GRADPSHS` determines the gradient of the potential pointwise at the location (r,φ,λ). The calculation is done in the Earth-fixed frame with non-singular formulas
# Input
* `field::Array{Float}`: a priori gravity field in |c\\s| or /s|c\\ format.
* `lambda::Array{Float}`: Longitude in radians.
* `theta::Array{Float}`:  Latitude in radians.
* `r::Float`: Radius in m.
* `max_lm::Integer (Optional)`: maximum degree/order (`Default: Inf`)
* `drtype::String (Optional)`:  defines the type of output and the possible values:
    * `lscs(Default)`: are the derivatives towards {x,y,z} but in the local spherical coordinate frame.
    * `xyz`: are the derivatives towards {x,y,z} in the cartesian frame.
* `jflag::Boolean (Optional)`: If True, reference field is subtracted. (default: True)
# Output
* `gradV::Array{Float}`: gradient [Vx Vy Vz] in [m/s^2].
# Example
```julia-repl
julia> gradpshs(sc,longitude,latitude,r,lmax = 360,drtype = "xyz")
┌ Warning: A reference field (WGS84) is removed from your coefficients
└ @ Main /path/to/your/file.jl:51
1×3 Array{Float64,2}:
 0.000316986  0.000800391  0.000202279
```
# Uses
- `Function`: normalklm,cs2sc,LegendreF,multmatvek.

"""
function gradpshs(field,lamRAD,phiRAD,r;lmax = Inf,constants = [],drtype = "lscs",jflag = 1)
#     print(field[:,241])
    ae = 6378137;
    GM = 3.986005e14;
    # preparing the input field
    (row, col) = size(field);
    #display(row)
    #display(col)
    if (row!=col) && (col!=2*row-1)
       error("Input field not in cs or sc format");
    elseif col==2*row-1
       field = sc2cs(field);
    end
    if (row-1 < lmax)
        lmax = row-1;
    end
    #display(field)
    field = field[1:lmax+1,1:lmax+1];
    #display(field)
    field = cs2sc(field,0);
    #display(field)
    row   = size(field,1);

    if (jflag == 1)
        field = field - cs2sc(normalklm(lmax));
        @warn "A reference field (WGS84) is removed from your coefficients"
    end
#     print(field[:,241])
    gradV = zeros(length(lamRAD),3);
    didx  = 1:length(lamRAD);
    r = (r .* ones(length(lamRAD),1))
    didx = reshape(didx, (length(didx), 1))
    lamRAD = reshape(lamRAD, (length(lamRAD), 1))
    phiRAD = reshape(phiRAD, (length(lamRAD), 1))
    didx  = didx[all(i -> i == 0, isnan.([lamRAD phiRAD r]), dims=2)];
#     print(didx)
    #display(~isnan.([lamRAD phiRAD r]))
    ##Calculation
    maxlength = 512;
    #process data
    parts = ceil(Int,length(didx)/maxlength);
#     print(field[:,241])
    for I = 1:parts
        # select the data
        # prepare cosine and sine --> cos(m*lam) and sin(m*lam) and  get
        # co-latitude
#         print(didx)
         idx = didx[(I-1)* maxlength+1:min(I*maxlength,length(didx))];
#         print(idx)
         theRAD  = pi/2 .- phiRAD[idx];

         l   = (0:lmax)'
#         print(l)
         TF  = ae./r[idx] .* ones(1,row);
#         print(TF)# matrix of size(length(r),length(n))
         TF  = broadcast(^,TF,l.+2);
#         print(TF)
        TSQ = sqrt.((2 .* l' .+ 1) ./ (2 .* l' .+ 3));
#         print(TSQ);
        TK  = GM ./ (2 .* ae .^ 2);
#         print(TK);
        #### ALL CORRECT TILL HERE IMO ######
        oPQ = [];
        pPQ = [];
        ocos = 0;
        pcos = 0;
        psin = 0;
        osin = 0;
        for m = 0:lmax
            l = (m:lmax);
            if (m == 0)
                pClm  = field[:,row+m] .* TSQ .* sqrt.((l .+ m .+ 1) .* (l .+ m .+ 2) .* 2);
                oClm  = field[:,row+m] .* TSQ .* sqrt.((l .- m .+ 1) .* (l .+ m .+ 1));
                #pPQ = [];
                #opQ = [];
                #display(size(TF));
                # pPQ   = TF .* (plm(l' .+ 1, m+1, theRAD));
                # oPQ   = TF.* (plm(l' .+ 1, m, theRAD));
                pPQ = (TF .* (LegendreF(l' .+ 1, m+1, theRAD)));
                oPQ = (TF .* (LegendreF(l' .+ 1, m, theRAD)));
                #display(oPQ);
                #####VERIFIED######
                pcos  = cos.((m .+ 1).*lamRAD[idx]);
                psin  = sin.((m .+ 1) .* lamRAD[idx]);
                ocos  = cos.(m .* lamRAD[idx]);
                #display(ocos);
                # ----------------------------
                #display(TK)
                #display(pPQ)
                #display(pClm);
                ######VERIFIED######
                gradV[idx,1] = (gradV[idx,1] .-      TK .* (pPQ*pClm) .* pcos);
                gradV[idx,2] = (gradV[idx,2] .-      TK .* (pPQ[:,:]*pClm[:,:]) .* psin);
                gradV[idx,3] = (gradV[idx,3] .- 2 .* TK .* (oPQ*oClm) .* ocos);
            elseif (m == 1)
                pClm  = field[m+1:end,row+m].*TSQ[m+1:end] .*sqrt.((l.+m.+1).*(l.+m.+2));
                pClm = reshape(pClm,(size(pClm)[1],1))
                oClm  = field[m+1:end,row+m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.+m.+1)) ;
                mClm  = field[m+1:end,row+m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.-m.+2).*2)  ;
                pSlm  = field[m+1:end,row-m].*TSQ[m+1:end].*sqrt.((l.+m.+1).*(l.+m.+2))     ;
                oSlm  = field[m+1:end,row-m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.+m.+1))     ;
                mSlm  = field[m+1:end,row-m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.-m.+2).*2)  ;
                mPQ   = oPQ[:,2:end];
                oPQ   = pPQ[:,2:end];
                #####
                pPQ   = TF[:, 2:end].*(LegendreF(l' .+ 1,m + 1,theRAD));
                #######
                mcos  = ocos;
                ocos  = pcos;
                pcos  = cos.((m+1).*lamRAD[idx]);
                osin  = psin;
                psin  = sin.((m+1).*lamRAD[idx]);
            ## ----------------------------
#                gradV[idx,1] = gradV[idx,1] .+ (TK .*(mPQ*mClm).*mcos);
                 gradV[idx,1] = gradV[idx,1] .+      TK .*(mPQ*mClm).*mcos  .-      TK.*(pPQ*pClm).*pcos  .-  TK.*(pPQ*pSlm).*psin;
#                 print(1)
                 gradV[idx,2] = gradV[idx,2] .-      TK .*(pPQ*pClm).*psin .+      TK.*(mPQ*mSlm).*mcos  .+  TK.*(pPQ*pSlm).*pcos;
                gradV[idx,3] = gradV[idx,3] .- 2 .* TK .*(oPQ*oClm).*ocos .- 2 .* TK.*(oPQ*oSlm).*osin;
            else
                pClm  = field[m+1:end,row+m].*TSQ[m+1:end].*sqrt.((l.+m.+1).*(l.+m.+2));

                oClm  = field[m+1:end,row+m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.+m.+1));
                mClm  = field[m+1:end,row+m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.-m.+2));
                pSlm  = field[m+1:end,row-m].*TSQ[m+1:end].*sqrt.((l.+m.+1).*(l.+m.+2));
                oSlm  = field[m+1:end,row-m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.+m.+1));
                mSlm  = field[m+1:end,row-m].*TSQ[m+1:end].*sqrt.((l.-m.+1).*(l.-m.+2));
                mPQ   = oPQ[:, 2:end];
                oPQ   = pPQ[:,2:end];

                #pPQ   = TF[:,m+1:end].*(LegendreF(l'.+1,m+1,theRAD));
                pPQ = (TF[:,m + 1:end] .* (LegendreF(l' .+ 1, m+1, theRAD)));
                mcos  = ocos;
                ocos  = pcos;
                pcos  = cos.((m+1).*lamRAD[idx]);
                msin  = osin;
                osin  = psin;
                psin  = sin.((m+1).*lamRAD[idx]);
                ## ----------------------------

                gradV[idx,1] = (gradV[idx,1] .+    (TK.*(mPQ * mClm).*mcos) .-    (TK .*(pPQ*pClm).*pcos)  .+  (TK .* (mPQ * mSlm).*msin)  .- (TK.*(pPQ*pSlm).*psin));
                gradV[idx,2] = (gradV[idx,2] .-    TK.*(mPQ*mClm).*msin .-    TK.*(pPQ*pClm).*psin  .+  TK.*(mPQ*mSlm).*mcos  .+ TK.*(pPQ*pSlm).*pcos);
                gradV[idx,3] = (gradV[idx,3] .- 2 .*TK.*(oPQ*oClm).*ocos .- 2 .*TK.*(oPQ*oSlm).*osin);
            end #for if else
        end#for m = 0:lmax
#         print(gradV)
        if (lowercase(drtype) == lowercase("LSCS"))
            cp = cos(phiRAD[idx]);
            sp = sin(phiRAD[idx]);
            cl = cos(lamRAD[idx]);
            sl = sin(lamRAD[idx]);
            gradV[idx,:] = multmatvek([cp.*cl,  cp.*sl,    sp,  -sp.*cl,  -sp.*sl,   cp,  -sl,  cl,  zeros(size(sp))],gradV[idx,:]);
        end
        if (length(drtype) == 9)
          gradV[idx,:] = multmatvek(drtype[idx,:],gradV[idx,:]);
        end
    end #end for I=1:parts
    return gradV
end #function end.
