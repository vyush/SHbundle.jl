using LinearAlgebra
"""
    cs2sc(field, backval = 0)
`cs2sc(field,backval)` converts the square (L + 1) x (L + 1) matrix `field`, containing spherical harmonics coefficient in |C\\S| storage foemat into a rectangular (L + 1) x (2L + 1) matrix in /S|C\\ format.
# Input
- `field::Array{Float,2}`: the square (L+1) x (L+1) matrix `field` , containing spherical harmonics coefficients in |C\\S| storage format.
-  `backval::Integer`: optional input and describes the matrix entries, where m > l. Default is 0.
# Output
- `sc::Array{Float,2}`: rectangular (L+1)x(2L+1) matrix in  /S|C\\format
# Examples
```julia-repl
julia> cs2sc([1 2 3;3 4 5;6 7 8])
3×5 Array{Float64,2}:
 0.0  0.0  1.0  0.0  0.0
 0.0  2.0  3.0  4.0  0.0
 5.0  3.0  6.0  7.0  8.0

julia> cs2sc([1 2 3;3 4 5;6 7 8],1)
3×5 Array{Float64,2}:
 1.0  1.0  1.0  1.0  1.0
 1.0  2.0  3.0  4.0  1.0
 5.0  3.0  6.0  7.0  8.0
```
#Uses
- `Package`: LinearAlgebra.
- `Function`: tril,triu,rotr90,reverse.
"""
function cs2sc(field, backval = 0)
    (rows,cols) = size(field);
    #display(cols)
    field = copy(field);
    if (rows!=cols) && (cols != 2*rows-1)
        error("Input neither in cs nor in sc format");
    elseif cols==2*rows-1
        sc = field;
    else
        lmax = rows-1;
        c    = tril(copy(field));
        #display(c)
        x = triu(copy(field),1)
        s    = rotr90(x);
        mask = backval .* ones(lmax+1,2*lmax+1);
        a = reverse(copy(triu(copy(mask[:,1:cols-1]))), dims = 2)
        b    = triu(copy(mask[:,cols:2*lmax+1]),1);
        sc   = [a b] .+ [s[:,2:lmax+1] c];
        #display(sc)
    end
    return sc;
end
