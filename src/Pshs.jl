using ProgressMeter
using SHbundle
"""
    pshs(field, lambdaRAD, phiRAD, r; constant = [], jflag = true,nargout = 1)
`PSHS` function determine the radial derivatives for the disturbing potential
T along the orbit. Also this file use spherical harmonics for
calculations it is especially designed for use with series of latitude
and longitude. So not a field is created but the derivates e.g. along a
orbit.

# INPUT:
- `field::Array{Float}`: a priori gravity field in cs or sc format
- `lambdaRAD::Array{Float}`: longitude in [rad]
- `phiRAD::Array{Float}`: latitude in [rad]
- `r::Array{Float}`:  radius in [m]
- `const::Array{Float}`: constant factors for GM and ae
- `jflag::Boolean`: reduce normal field (default: true)
- `nargout::Integer`:Numbrt of radial derivatives (Default: 1)

# OUTPUT:
-   `T::Array{Float}`: distrubing potentia
-   `Tr::Array{Float}`: first radial derivative
-   `Trr::Array{Float}`: second radial derivative
-   `Trrr::Array{Float}`: third radial derivative
-   `Trrrr::Array{Float}`: fourth radial derivative

# Example:
```julia-repl
julia> pshs(sc,0:0.1:0.5,0:0.1:0.5,7000000)
┌ Warning: A reference field (grs80) is removed from your coefficients
└ @ Main In[51]:58
6-element Vector{Float64}:
 140.63447667985835
 145.98203226869023
 134.37204563711867
 125.19362060224029
 127.63946461208138
 121.24626457365937
```

# Uses
- `Function`: normalklm,cs2sc,LegendreF.

"""

function pshs(field, lambdaRAD, phiRAD, r; constant = [], jflag = true,nargout = 1)
    # load necessary constants
    if(isempty(constant))
            ae     = 6378137;
            GM     = 3.986005e14;
            constant = [GM ae];
        elseif length(constant) == 2
            GM = constant[1];
            ae = constant[2];
        else
            error("Please verify the input constants.");
    end


    (row,col) = size(field)
    if ((row != col) && (col != 2*row-1))
        error("Input field not in cs or sc format")
    end
    field = cs2sc(field)
    lmax = row-1

    # prepare the coordinates
    theta = (pi/2 .- phiRAD);
    didx   = 1:length(lambdaRAD);

    # substract reference field
    if (jflag)
        field = field - cs2sc(normalklm(lmax,typ = "grs80"));
        @warn("A reference field (grs80) is removed from your coefficients")
    end

    ## PREPARATION
    T = zeros(size(lambdaRAD));
    if (nargout > 1)
        Tr    = zeros(size(lambdaRAD));
    end
    if (nargout > 2)
        Trr   = zeros(size(lambdaRAD));
    end
    if (nargout > 3)
        Trrr  = zeros(size(lambdaRAD));
    end
    if (nargout > 4)
        Trrrr = zeros(size(lambdaRAD));
    end

    maxlength = 5000;

    # prepare index
    #     idx   = all(!isnan([lambdaRAD theta r]),2);
    r = (r .* ones(length(lambdaRAD),1))
    didx = reshape(didx, (length(didx), 1))
    lambdaRAD = reshape(lambdaRAD, (length(lambdaRAD), 1))
    theta = reshape(theta, (length(theta), 1))
    idx = all(i -> i == 0, isnan.([lambdaRAD theta r]), dims=2)
    didx  = didx[idx];
    lambdaRAD   = lambdaRAD[idx];
    theta   = theta[idx];
    r     = r[idx];
    parts = ceil(length(lambdaRAD)/maxlength);
    @showprogress 1 "Computing..." for I = 1:floor(Int,parts)

        N = min.(I*maxlength, length(lambdaRAD));
        idx = ((I-1)*maxlength+1:N)';
        m       = (0:row-1);
        m = reshape(m, (length(m), 1))

        l       = m';

        mlam    = m * (lambdaRAD[idx]);
        # matrix of size(length(m),length(lam))
        cosinus = cos.(mlam);
        sinus   = sin.(mlam);
        n  = m.*ones(length(m),length(idx));
        n = n'

        TF = (ae ./ r[idx])' * ones(1,row);

        TK     = GM./r[idx];
        TFn    = TF.^n;

        if (nargout >= 2)
            TrK    = -GM./r[idx].^2;
            TrF    = (n+1).*(TF.^n);
        end      # factors for Tr
        if (nargout >= 3)

            TrrK   = -TrK./r[idx];
            TrrF   = (n+2).*TrF;
        end      # factors for Trr
        if (nargout >= 4)
            TrrrK  = -TrrK./r[idx];
            TrrrF  = (n+3).*TrrF;
        end      # factors for Trrr
        if (nargout == 5)
            TrrrrK = -TrrrK./r[idx];
            TrrrrF = (n+4).*TrrrF;
        end      # factors for Trrrr

        ## CALCULATION
        TA = zeros(length(idx),row);
        TB = zeros(length(idx),row);
        if (nargout >=2)
            TrA    = zeros(length(idx),row);
            TrB    = zeros(length(idx),row);
        end
        if (nargout >=3)
            TrrA   = zeros(length(idx),row);
            TrrB   = zeros(length(idx),row);
        end
        if (nargout >=4)
            TrrrA  = zeros(length(idx),row);
            TrrrB  = zeros(length(idx),row);
        end
        if (nargout >=5)
            TrrrrA = zeros(length(idx),row);
            TrrrrB = zeros(length(idx),row);
        end


        for m = 0:row-1
            if (m==0)
                Cnm = field[:,row+m];
                P = LegendreF(l, m, theta[idx]);

                # ------------------------------------------------------------------
                TP      = TFn .* P;             # calc for fourth radial derivative
                TA[:,1] = TP * Cnm;
                # ----------------------------
                if (nargout >= 2)
                    TrP         = TrF.*P;     # calc for first radial derivative
                    TrA[:,1]    = TrP*Cnm;
                end
                # ----------------------------
                if (nargout >= 3)
                    TrrP        = TrrF.*P;    # calc for second radial derivative
                    TrrA[:,1]   = TrrP*Cnm;
                end

                # -----------------------------
                if (nargout >= 4)
                    TrrrP       = TrrrF.*P;   # calc for third radial derivative
                    TrrrA[:,1]  = TrrrP*Cnm;
                end
                # ------------------------------------------------------------------
                if (nargout == 5)
                    TrrrrP      = TrrrrF.*P;  # calc for fourth radial derivative
                    TrrrrA[:,1] = TrrrrP*Cnm;
                end
                # ------------------------------------------------------------------
            else
                Cnm = field[m+1:end,row+m];        # get Cnm coefficients for order m
                Snm = field[m+1:end,row-m];        # get Snm coefficients for order m
                P = LegendreF(l', m, theta[idx]);
                P = P[:,m+1:end];

                # ------------------------------------------------------------------
                TP     = TFn[:,m+1:end].*P;        # calc for potential
                TA[:,m+1] = TP*Cnm;
                TB[:,m+1] = TP*Snm;
                # ------------------------------------------------------------------
                if (nargout >= 2)
                    TrP           = TrF[:,m+1:end].*P;        # calc for first radial derivative
                    TrA[:,m+1]    = TrP*Cnm;
                    TrB[:,m+1]    = TrP*Snm;
                end
                # ------------------------------------------------------------------
                if (nargout >= 3)
                    TrrP          = TrrF[:,m+1:end].*P;   # calc for second radial derivative
                    TrrA[:,m+1]   = TrrP*Cnm;
                    TrrB[:,m+1]   = TrrP*Snm;
                end
                # ------------------------------------------------------------------
                if (nargout >= 4)
                    TrrrP         = TrrrF[:,m+1:end].*P;  # calc for third radial derivative
                    TrrrA[:,m+1]  = TrrrP*Cnm;
                    TrrrB[:,m+1]  = TrrrP*Snm;
                end
                # ------------------------------------------------------------------
                if (nargout == 5)
                    TrrrrP        = TrrrrF[:,m+1:end].*P; # calc for fourth radial derivative
                    TrrrrA[:,m+1] = TrrrrP*Cnm;
                    TrrrrB[:,m+1] = TrrrrP*Snm;
                end
                # ------------------------------------------------------------------
            end
        end
        T[didx[idx]] = TK' .* sum(TA.*cosinus'+TB.*sinus',dims = 2);
        if (nargout >= 2)
            Tr[didx[idx]]    = TrK' .*sum.(TrA.*cosinus'+TrB.*sinus',dims = 2);
        end
        if (nargout >= 3)
            Trr[didx[idx]]   = TrrK' .*sum(TrrA.*cosinus'+TrrB.*sinus',dims = 2);
        end
        if (nargout >= 4)
            Trrr[didx[idx]]  = TrrrK' .*sum(TrrrA.*cosinus'+TrrrB.*sinus',dims = 2);
        end
        if (nargout == 5)
            Trrrr[didx[idx]] = TrrrrK' .*sum(TrrrrA.*cosinus'+TrrrrB.*sinus',dims = 2);
        end
    end
    if(nargout == 1)
        return T;
    end
    if (nargout == 2)
        return (T,Tr);
    end
    if (nargout ==3)
            return(T,Tr,Trr)
    end
    if (nargout == 4)
        return (T,Tr,Trr,Trrr);
    end
    if (nargout == 5)
        return(T,Tr,Trr,Trrr,Trrrr);
    end

end
