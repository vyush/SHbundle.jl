using Xnumber
"""
  legendreF_xnum(lmax,order,degree,theta)
"""
function legendreF_xnum(lmax,order,degree,theta)
  Wmm    = zeros(order + 1);
  Wlm_1  = zeros(lmax - order + 1);
  Wlm_2  = zeros(lmax - order);
  (Wmm,Wlm_1,Wlm_2) = init_legenre_1st_kind_plm(lmax,order)
  nL = zeros(length(theta),length(degree))
  dnL = zeros(length(theta),length(degree))
  ddnL = zeros(length(theta),length(degree))
  degreelen = length(degree);
  thetalen = length(theta);
  matsiz1 = thetalen;
  matsiz2 = degreelen;
  numlp  = (lmax - order + 1);
  if (length(nL)>0)
      L = zeros(numlp * (thetalen + 1));
  end
  if (length(dnL)>0)
      dL = zeros(numlp * (thetalen + 1));
  end
  if (length(ddnL)>0)
      ddL = zeros(numlp * (thetalen + 1));
  end
  if (length(nL)>0 && length(dnL) == 0 && length(ddnL) == 0)
    # print(1);
    for t = 0:thetalen-1
        ct = cos(theta[t+1]);
        st = sin(theta[t+1]);

        id_mm = id_lm_1 = id_lm_2 = 0;
        id_x = t * numlp;

        x0 = xnumber(1.0 , 0);
        for m in 1:order
            x0 = x_norm(x_mult(Wmm[id_mm + 1] * st, x0));
            id_mm = id_mm + 1
        end
        m = order + 1
        L[1 + id_x] = x_x2dbl(x0);
        id_x = id_x + 1
        if  (m <= lmax)
            x1 = x_norm(x_mult(Wlm_1[id_lm_1 + 1]*ct ,x0));
            id_lm_1 = id_lm_1 + 1;
            L[id_x + 1] = x_x2dbl(x1);
            id_x = id_x + 1;
        end

        for l = (m + 1):lmax
            x2 = x_norm(x_multadd(Wlm_1[id_lm_1+1] * ct, x1, -Wlm_2[id_lm_2+1], x0));
            id_lm_1 = id_lm_1 + 1;
            id_lm_2 = id_lm_2 + 1;
            L[id_x+1] = x_x2dbl(x2);
            id_x = id_x + 1
            x0 = x1; x1 = x2;
        end
        l = lmax + 1
    end
    id_x = 0;
    for l = 0 : (degreelen - 1)
        for t = 0 : (thetalen - 1)
            if ((degree[l+1]<order) || (degree[l+1]<0))
                nL[id_x + 1] = 0;
            else
                id_mm = t * numlp + degree[l+1] - order;
                nL[id_x + 1] = L[id_mm+ 1]
            end
            id_x = id_x + 1
        end
    end
    return (nL)
  elseif (length(nL)>0 && length(dnL) > 0 && length(ddnL) == 0)
    for t = 0:thetalen-1
      ct = cos(theta[t+1]);
      st = sin(theta[t+1]);

      id_mm = id_lm_1 = id_lm_2 = 0;
      id_x = t * numlp;

      dx0 = xnumber(0.0, 0);
      x0  = xnumber(1.0, 0);
      for m = 1:order
        iWmm = Wmm[id_mm + 1];
        id_mm = id_mm + 1;
        dx0 = x_norm(x_mult(iWmm, x_multadd(st, dx0, ct, x0)));
        x0  = x_norm(x_mult(iWmm * st, x0));
      end
      dL[id_x + 1] = x_x2dbl(dx0);
      L[id_x + 1]  = x_x2dbl(x0);
      id_x=id_x+1
      m = order + 1;
      if (m <= lmax)
        iWlm_1 = Wlm_1[id_lm_1 + 1];
        id_lm_1 = id_lm_1 + 1
        dx1 = x_norm(x_mult(iWlm_1, x_multadd(ct, dx0, -st, x0)));
        x1  = x_norm(x_mult(iWlm_1 * ct, x0));
        dL[id_x + 1] = x_x2dbl(dx1);
        L[id_x + 1]  = x_x2dbl(x1);
        id_x = id_x + 1;
      end


      for l = (m + 1):lmax
        iWlm_1 = Wlm_1[id_lm_1 + 1]; iWlm_2 = Wlm_2[id_lm_2 + 1];
        id_lm_1 = id_lm_1 + 1;
        id_lm_2 = id_lm_2 + 1;
        dx2 = x_norm(x_multadd(iWlm_1, x_multadd(ct, dx1, -st, x1), -iWlm_2, dx0));
        x2  = x_norm(x_multadd(iWlm_1 * ct, x1, -iWlm_2, x0));
        dL[id_x + 1] = x_x2dbl(dx2);
        L[id_x + 1]  = x_x2dbl(x2);
        id_x=id_x+1;
        dx0 = dx1; dx1 = dx2;
        x0 = x1; x1 = x2;
      end
    end
    id_x = 0;
    for l = 0:degreelen-1
      for t = 0:thetalen-1
        if ((degree[l+1] < order) || (degree[l+1] < 0))
          dnL[id_x+1] = nL[id_x+1] = 0.0;
        else
          id_mm = t * numlp + degree[l+1] - order;
          dnL[id_x+1] = dL[id_mm+1];
          nL[id_x+1] = L[id_mm+1];
        end
        id_x = id_x + 1
      end
    end
    return (nL,dnL)
  elseif (length(nL)>0 && length(dnL) > 0 && length(ddnL) > 0)
    for t = 0:thetalen - 1
      ct = cos(theta[t+1])
      st = sin(theta[t+1])

      id_mm = id_lm_1 = id_lm_2 = 0
      id_x = t*numlp;

      ddx0 = xnumber(0,0);
      dx0 = xnumber(0,0);
      x0 = xnumber(1,0);
      for m = 1:order
        iWmm = Wmm[id_mm + 1]
        id_mm = id_mm + 1;
        ddx0 = x_norm(x_mult(iWmm, x_multadd(st, x_sub(ddx0, x0), 2.0 * ct, dx0)));
        dx0  = x_norm(x_mult(iWmm, x_multadd(st, dx0, ct, x0)));
        x0   = x_norm(x_mult(iWmm * st, x0));
      end
      m = order + 1
      ddL[id_x+1] = x_x2dbl(ddx0);
      dL[id_x+1]  = x_x2dbl(dx0);
      L[id_x+1]   = x_x2dbl(x0);
      id_x= id_x + 1;

      if (m<=lmax)
        iWlm_1 = Wlm_1[id_lm_1 + 1];
        id_lm_1= id_lm_1 + 1
        ddx1 = x_norm(x_mult(iWlm_1, x_multadd(ct, x_sub(ddx0, x0), -2.0 * st, dx0)));
        dx1  = x_norm(x_mult(iWlm_1, x_multadd(ct, dx0, -st, x0)));
        x1   = x_norm(x_mult(iWlm_1 * ct, x0));
        ddL[id_x + 1] = x_x2dbl(ddx1);
        dL[id_x + 1]  = x_x2dbl(dx1);
        L[id_x + 1]   = x_x2dbl(x1);
        id_x = id_x + 1;
      end
      for l = (m + 1):lmax
        iWlm_1 = Wlm_1[id_lm_1+1]; iWlm_2 = Wlm_2[id_lm_2+1];
        id_lm_1 = id_lm_1 + 1;
        id_lm_2 = id_lm_2 + 1;
        ddx2 = x_norm(x_multadd(iWlm_1, x_multadd(ct, x_sub(ddx1, x1), -2.0 * st, dx1), -iWlm_2, ddx0));
        dx2  = x_norm(x_multadd(iWlm_1, x_multadd(ct, dx1, -st, x1), -iWlm_2, dx0));
        x2   = x_norm(x_multadd(iWlm_1 * ct, x1, -iWlm_2, x0));
        ddL[id_x+1] = x_x2dbl(ddx2);
        dL[id_x+1]  = x_x2dbl(dx2);
        L[id_x+1]   = x_x2dbl(x2);
        id_x = id_x+1;
        ddx0 = ddx1; ddx1 = ddx2;
        dx0 = dx1; dx1 = dx2;
        x0 = x1; x1 = x2;
      end
    end
    id_x = 0;
    for l = 0:degreelen-1
      for t = 0:thetalen-1
        if ((degree[l+1]<order) || (degree[l+1]<0))
          ddnL[id_x + 1] = dnL[id_x + 1] = nL[id_x+1] = 0
        else
          id_mm = t * numlp + degree[l+1] - order;
          ddnL[id_x+1] = ddL[id_mm+1];
          dnL[id_x+1]  = dL[id_mm+1];
          nL[id_x+1]   = L[id_mm+1];
        end
        id_x = id_x + 1
      end
    end
    return (nL,dnL,ddnL)
  else
    error("Chosen combination of output variables nL, dnL, ddnL is not allowed!");
  end
end
