function secrecur(m,y)
    if m == 0
       fac = 1;
    else
       mm  = 2 .* (1:m);
       fac = sqrt.(2*prod((mm.+1)./mm));
    end
    out = fac*y.^m;             # The 1st column of ptmp.
    return out
end





function lrecur(in,x,m,lmax)
    for l = m+1:lmax
       col   = l - m + 1;# points to the next column of ptmp
       root1 = sqrt( (2 .*l.+1).*(2 .*l.-1)/((l .-m).*(l.+m)) ) ;
       root2 = sqrt( (2 .*l.+1).*(l.+m.-1).*(l.-m.-1) / ( (2 .*l.-3).*(l.-m).*(l.+m) ) );

       # recursion
       if (l == m+1)
           in[:,col] = root1 .*x.*in[:,col-1];
       else
           in[:,col] = root1 .*x.*in[:,col-1] - root2 *in[:,col-2];
       end
    end
    return in
end






function derivALF(in,miin,plin,m,lmax)
    l = m:lmax+1;
    if (m == 0)
        in[:,1] .= 0;
        if (lmax > m)
            in[:,2:end] = broadcast(*,plin',-sqrt.((l[2:end].+1).*l[2:end]./2))';
        end
    elseif (m == 1)
        in[:,1] = miin[:,2];
        if (lmax > m)
            in[:, 2:end] =  broadcast(*, miin[:,3:end]', sqrt.((l[2:end] .+ 1) .* l[2:end] ./ 2))' .- 0.5 .* broadcast(*,plin', sqrt.((l[2:end] .- 1) .* (l[2:end].+2)))';
        end
    elseif (m == lmax)

        in[:,1] = sqrt.(m./2).*miin[:,2];

    else

        in[:,1] = sqrt.(m./2).*miin[:,2];
        if (lmax > m)
            in[:,2:end] = 0.5.*broadcast(*,miin[:,3:end]',sqrt.((l[2:end].+m).*(l[2:end].-m.+1)))' - 0.5 .* broadcast(*,plin',sqrt.((l[2:end].-m).*(l[2:end].+m.+1)))';
        end
    end
    return in
end



"""
    LegendreF(l, m, thetaRAD;nret = 1)

PLM Fully normalized associated Legendre functions for a selected order M.
# Input
- `l::Array{Integer}`: Degree.For l < m a vector of zeros will be returned.
- `m::Integer`: Order.
- `thetaRAD::Array{Float}`: Co-Latitude in radians.
- `nret::Integer`:Number of derivatives - 1. (Default = 1)
# Output
- `p::Array{Float}`:Matrix with Legendre functions. The matrix has length(thetaRAD) rows and length(l) columns.
- `dp::Array{Float}`:Matrix with first derivative of Legendre functions. The matrix has length(thetaRAD) rows and length(l) columns.
- `ddp::Array{Float}`:Matrix with second derivative of Legendre functions. The matrix has length(thetaRAD) rows and length(l) columns.


# Examples
```julia-repl
julia> LegendreF(6,5,0:1,nret = 2)
([0.0; 1.9123432921847003], [0.0; 3.161209319776283])
```
# Uses
- `Functions` : derivALF,lrecur,secrecur,reshape.
"""
function LegendreF(l, m, thetaRAD;nret = 1)
    if (length(l) == 1)
        l = [l[1]];
    end
    if (min(size(l,1),size(l,2)) != 1)
        error("Degree l must be vector (or scalar)")
    end
    if (any(l .- floor.(l) .!= 0))
        error("Vector l contains non-integers.")
    end
    if (length(m) != 1)
        error("Order m must be scalar.")
    end
    if (m - floor(m) != 0)
        error("Order m must be integer.")
    end


    #negative orders:
    msign = 1;
    if (m<0)
        msign = (-1)^m
    end
    m= abs(m);


    # Preliminaries.
    lcol = size(l,2);
    trow = size(thetaRAD,1);
    lmax = maximum(l);
    if (lmax < m)
        p = zeros(length(thetaRAD),length(l));
        dp = zeros(length(thetaRAD),length(l));
        ddp = zeros(length(thetaRAD),length(l));
    end
    n    = length(thetaRAD);				# number of latitudes
    t    = thetaRAD[:];
    x    = cos.(t);
    y    = sin.(t);
    lvec = l[:]';					# l can be used now as running index.

    # if ((minimum(t) < -1e-14) || (maximum(t) - pi) > 1e-14))
    #     warning('Is the co-latitude ''thetaRAD'' given in radian?')
    # end

    # Recursive computation of the temporary matrix ptmp, containing the Legendre
    # functions in its columns, with progressing degree l. The last column of
    # ptmp will contain zeros, which is useful for assignments when l < m.
    ptmp  = zeros(n,lmax-m+2);
    # first derivative needs also P_{n,m+1} and P_{n,m-1}
    ptmp_m1 = zeros(n,lmax-m+3);
    ptmp_p1 = zeros(n,lmax-m+1);
    dptmp   = zeros(n,lmax-m+2);

    # second derivative needs also dP_{n,m+1} and dP_{n,m-1}
    dptmp_m1 = zeros(n,lmax-m+3);
    dptmp_p1 = zeros(n,lmax-m+1);
    ptmp_m2  = zeros(n,lmax-m+4); # but these first derivative need dP_{n,m+2} and dP_{n,m-2}
    ptmp_p2  = zeros(n,lmax-m);
    ddptmp   = zeros(n,lmax-m+2);


    #--------------------------------------------------------------------
    # sectorial recursion: PM (non-recursive, though)
    #--------------------------------------------------------------------
    ptmp[:,1] = secrecur(m,y);
    # frist derivative needs preceding and subsequent element
    if (m > 0)
        ptmp_m1[:,1] = secrecur(m-1,y);
    end     # preceding elements
    if (m < lmax)
        ptmp_p1[:,1] = secrecur(m+1,y);
    end     # subsequent elemtens
    # second derivative needs P_{n,m+2} and P_{n,m-2} as well
    if (m > 1)
        ptmp_m2[:,1] = secrecur(m-2,y);
    end   # preceding elements
    if (m < lmax-1)
        ptmp_p2[:,1] = secrecur(m+2,y);
    end   # subsequent elemtens

    #--------------------------------------------------------------------
    # l-recursion: P
    #--------------------------------------------------------------------
    ptmp = lrecur(ptmp,x,m,lmax);
    if (m > 0)
        ptmp_m1 = lrecur(ptmp_m1,x,m-1,lmax);
    end    # preceding elements
    if (m < lmax)
        ptmp_p1 = lrecur(ptmp_p1,x,m+1,lmax);
    end    # subsequent elemtens
    if (m > 1)
        ptmp_m2 = lrecur(ptmp_m2,x,m-2,lmax);
    end  # preceding elements
    if (m < lmax-1)
        ptmp_p2 = lrecur(ptmp_p2,x,m+2,lmax);
    end

    #--------------------------------------------------------------------
    # now compute the derivatives
    #--------------------------------------------------------------------
    dptmp = derivALF(dptmp,ptmp_m1,ptmp_p1,m,lmax);
    # second derivative
        if (m > 0)
            dptmp_m1 = derivALF(dptmp_m1,ptmp_m2,ptmp,m-1,lmax);
        end
        if (m < lmax)
            dptmp_p1 = derivALF(dptmp_p1,ptmp,ptmp_p2,m+1,lmax);
        end
        ddptmp = derivALF(ddptmp,dptmp_m1,dptmp_p1,m,lmax);

    #--------------------------------------------------------------------
    # The Legendre functions have been computed. What remains to be done, is to
    # extract the proper columns from ptmp, corresponding to the vector lvec.
    # If l or thetaRAD is scalar the output matrix p reduces to a vector. It should
    # have the shape of respectively thetaRAD or l in that case.
    #--------------------------------------------------------------------
    lind       = all(i-> i< m,lvec, dims=1); # index into l < m
    pcol       = lvec .- m .+ 1;			        # index into columns of ptmp
    pcol[lind] = (lmax-m+2) .* ones(sum(lind),1);	# Now l < m points to last col.
    p          = msign * ptmp[:,pcol];			# proper column extraction + handeling of negative order
    dp =  msign * dptmp[:,pcol];            # proper column extraction + handeling of negative order

    ddp = msign * ddptmp[:,pcol];           # proper column extraction + handeling of negative order
    p = reshape(p,(size(p)[1],size(p)[3]));
    dp = reshape(dp,(size(dp)[1],size(dp)[3]));
    ddp = reshape(ddp,(size(ddp)[1],size(ddp)[3]));
    if (nret == 1)
        return p
    elseif (nret == 2)
        return p,dp
    elseif(nret == 3)
        return (p,dp,ddp)
    else
        error("Dont have that many outputs")
    end
end
