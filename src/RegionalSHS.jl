"""
    regionalshs(klm, gridext;lmax = Inf,quant = "potential",grid = "mesh",dlat = Inf,dlong = Inf,height = 0,ellipsoid = "grs80",sub_normal = true)
`REGIONALSHS` spherical harmonic synthesis on any regular regional grid.
# Input
- `field::Array{Float}`: set of SH coefficients, either in SC/CS formats.
- `gridext::Array{Float}`: extent of the grid in radians.[co-lat_min co-lat_max long_min long_max] where co-lat(itude) long(itude).
- `lmax::Integer`:  maximum degree/order (default: determined from klm).
- `quant::String`: optional string argument, defining the field quantity:
    - `potential(default)`:  (default), potential [m^2/s^2],
    - `tr`: grav. disturbance, 1st rad. derivative [mGal],
    - `trr`: 2nd rad. derivative [E],
    - `none`: coefficients define the output
    - `geoid`: geoid height [m],
    - `dg`, `gravity`: gravity anomaly [mGal],
    - `slope`: size of surface gradient [arcsec],
    - `water`: equiv. water height [m],
    - `smd`: surface mass density [kg/m^2].
    - `grid::String` ...... optional string argument, defining the grid:
        - `pole`, `mesh`: (default), equi-angular (n+1)*2n, including poles and Greenwich meridian.
        - `block`, `cell`: equi-angular block midpoints. n*2n.
- `dlat::Float`: grid size in the latitude direction in radians (def.: pi/(2*lmax))
- `dlong::Float`: grid size in the longitude direction in radians (def.: pi/(2*lmax))
- `height::Float`: height above Earth mean radius [m](Default: 0).
- `sub_normal::Boolean`: if set, subtracts normal gravity field of the reference ellipsoid (default: true)
-  `ellipsoid::String`: Reference ellipsoid
                  - 'wgs84'
                  - 'grs80'
                  - 'he'
# Output
- `f::Array{Float}`: The global field.
- `th::Array{Float}`: vector of co-latitudes in radians.
- `lam::Array{Float}`: vector of longitudes in radians.
# Example
```julia-repl
julia> regionalshs(sc,[0,0.5,0,0.5])
┌ Warning: A reference field (grs80) is removed from your coefficients
└ @ Main In[47]:67
([146.76098702197197 146.76098702197197 … 146.76098702197197 146.76098702197197; 156.25464669843151 156.1961696137779 … 154.2375859520772 154.1875422093218; … ; 484.16547533706 478.6554240680784 … 184.14349243051214 181.36625241858016; 486.254960268093 478.15975157556556 … 176.9040317586219 175.87677551579014], 0.0:0.01308996938995747:0.4974188368183839, [0.0 0.01308996938995747 … 0.4843288674284264 0.4974188368183839])
```
# Uses
- `Function`: normalklm,cs2sc,LegendreF,eigengrav.

"""
using SHbundle
using ProgressMeter
function regionalshs(klm, gridext;lmax = Inf,quant = "potential",grid = "mesh",dlat = Inf,dlong = Inf,height = 0,ellipsoid = "grs80",sub_normal = true)
    # field size determination, rearrange field and subtract reference field
    (rw,cols) = size(klm);

    if rw == cols              # klm is in CS-format
       klm = cs2sc(klm);       # convert to SC-format
    elseif (cols != 2*rw-1)      # klm is in SC-format already
       error("Input field not in cs or sc format");
    end
    if (lmax > (rw - 1))     # desired lmax is bigger than what the field provides
        lmax = rw - 1;     # adjust lmax
    elseif (lmax < (rw - 1)) # if lmax is smaller than what the field provides
        klm = klm[1:(lmax+1), (rw - lmax):(rw + lmax)]; # adjust field
    end

    # -------------------------------------------------------------------------
    # Grid definition.
    # -------------------------------------------------------------------------

    if (!(isa(grid, Union{AbstractString,Char})))
        error("grid argument must be string")
    end
    thmin   = gridext[1]; thmax   = gridext[2];
    lammin  = gridext[3]; lammax  = gridext[4];
    # Grid size in latitude direction
    if isinf(dlat)
        if isinf(dlong)
            dt = pi/lmax;
        else
            dt = dlong;
        end
    else
        dt = dlat;
    end
    # Grid size in longitude direction
    if isinf(dlong)
        dlam = dt;
    else
        dlam = dlong;
    end

    if (lowercase(grid) == "pole" || lowercase(grid) == "mesh")
        th   = (thmin:dt:thmax);
        lam  = (lammin:dlam:lammax)';
    elseif (lowercase(grid) == "block" || lowercase(grid) == "cell")
        th   = (thmin+dt/2:dt:thmax);
        lam  = (lammin+dlam/2:dlam:lammax)';
    else
        error("Dont know about this type of grid yet.")
    end

#     (tempx,tempy) = size(th);
#     nlat = max(tempx,tempy);
#     (tempx,tempy) = size(lam);
#     nlon = max(tempx,tempy);
    nlat = length(th);
    nlon = length(lam)

    # -------------------------------------------------------------------------
    # Preprocessing on the coefficients:
    #    - subtract reference field (if jflag is set)
    #    - specific transfer
    #    - upward continuation
    # -------------------------------------------------------------------------
    if (sub_normal)
        klm = klm - cs2sc(normalklm(lmax, typ = ellipsoid));
        @warn("A reference field ("* ellipsoid *") is removed from your coefficients")
    end

    l       = 0:lmax
    transf  = eigengrav(l, fstr = quant, h = height);

    klm     = diagm(transf) * klm;



    a = zeros(nlat,lmax+1);
    b = zeros(nlat,lmax+1);

    #-------------------------
    # Treat m = 0 separately
    #-------------------------
    p = LegendreF(0:lmax,0,th)
    a[:,1] = p*klm[1:lmax+1,lmax+1]
    #----------------------
    # Do loops over orders
    #----------------------
    @showprogress 1 "Computing..." for m = 1:lmax
        p = LegendreF(m:lmax,m,th)
        a[:, m+1] = p * klm[m+1:lmax+1, lmax+1+m];
        b[:, m+1] = p * klm[m+1:lmax+1, lmax+1-m];
    end
    # -------------------------------------------------------------------------
    # The second synthesis step consists of an inverse Fourier transformation
    # over the rows of a and b.
    # In case of 'block', the spectrum has to be shifted first.
    # -------------------------------------------------------------------------
    if (grid == "block" || grid ==  "cell")
       m      = (0:lmax)';
       # '
       cshift = ones(nlat, 1) .* cos.(m .* pi./2/(pi/dlam));	# cshift./sshift describe the
       sshift = ones(nlat, 1) .* sin.(m.*pi./2/(pi/dlam));	# half-blocksize lambda shift.
       atemp  =  cshift.*a + sshift.*b;
       b      = -sshift.*a + cshift.*b;
       a      = atemp;
    end

    c = cos.((0:lmax) .* lam);
    s = sin.((0:lmax) .* lam);
    f = a*c + b*s;
    return f,th,lam;
end
