using SHbundle
"""
    gshs_ptw(field,lamRAD,phiRAD,r;max_lm = Inf, quant = "potential", sub_grs80 = true)

`GSHS_PTW` calculates a pointwise global spherical harmonic synthesis.
# Input
- `field::Array{Float}`: gravity field in |c\\s| or /s|c\\ format.
- `lamRAD::Array{Float}`: longitude in radians.
- `thetaRAD::Array{Float}`: Latitude in radians.
- `max_lm::Integer`: Maximum degree.(Default = Max degree of field).
- `quant::String`: Defines the field quantity:(Default = potential)
    - `potential` : Potential.
    - `distrurbance` : gravity disturbance.
    - `dg,ganomaly` : Gravity anomaly.
    - `gradient` : 2nd rad. derivative [E].
    - `geoid` : geoid height [m].
- sub_grs80::Boolean: determines whether GRS80 is subtracted.(Default = True).
# Output
- `f::Array{Float}`:field quantity.

# Examples
```julia-repl
julia> gshs_ptw(sc, longitude, latitude, [r],max_lm = 240, quant = "potential")
1-element Vector{Float64}:
 -536.031179075617
```
# Uses
- `Functions` : cs2sc,sc2cs,lovenr,LegendreF.
"""
function gshs_ptw(field,lamRAD,phiRAD,r;max_lm = Inf, quant = "potential", sub_grs80 = true)

    row,col = size(field);
#     gm = astronomical_constants("Constant of gravitation")[1];
#     a_E = astronomical_constants("Equatorial radius of the Earth")[1];
    # field size determination, rearrange field and subtract reference field
    a_E = 6378137;
    gm = 3.986005e14;
    if(col == row)
        field = cs2sc(field);
    elseif (col != 2*row - 1)
        error("Input "*field*" not in cs or sc format");
    end
    #verified


    if(max_lm > row - 1)
        max_lm = row - 1;
    elseif max_lm < row - 1
        field = field[1:max_lm +  1 , row - max_lm : row + max_lm]
        row = size(field,1);
    end

    if (sub_grs80)
        field = field .- cs2sc(Array(normalklm(max_lm)));
    end
    #verified

    theRAD = (pi/2 .- phiRAD)
    num_coord = length(lamRAD)
    didx = 1:num_coord;
    l = [0:max_lm;]
    m = l';
    l_trans = l';
    #another assumption
    chunklength = 120000/max_lm;
    ###############
    # apply transfer function
    if(quant == "potential")
        if isnan(gm)
            error("GM not defined!");
        end;
        tk = gm ./ a_E;
        tf = ones(size(l));
        tl = l_trans .+ 1;
    elseif(quant == "distrurbance")
        if (isnan(gm))
            error("GM not defined!");
        end
        tk = -gm / a_E / a_E * 1e5;   # [mGal]
        tf = l .+ 1;
        tl = l_trans .+ 2;
    elseif(quant == "ganomaly" || quant == "dg")
        if (isnan(gm))
            error("GM not defined!");
        end
        tk = gm / a_E / a_E * 1e5;    # [mGal]
        tf = l .- 1;
        tl = l_trans .+ 2;
     elseif(quant == "gradient")
        if (isnan(gm))
            error("GM not defined!");
        end
        tk = gm / a_E / a_E / a_E * 1e9; # [E]
        tf = (l .+ 1) .* (l .+ 2);
        tl = l_trans .+ 3;
    elseif(quant == "geoid")
        tk = a_E;                            # [m]
        tf = ones(size(l));
        tl = l_trans .- 1;
    else
        error("Requested functional QUANT not available.");
    end
    #verified



    field = field .* ((tk .* tf[:]) .* ones(1, 2 * max_lm + 1));
    f = zeros(size(lamRAD))

    r = (r .* ones(length(lamRAD),1))
    didx = reshape(didx, (length(didx), 1))
    lamRAD = reshape(lamRAD, (length(lamRAD), 1))
    theRAD = reshape(theRAD, (length(theRAD), 1))
    idx = all(i -> i == 0, isnan.([lamRAD theRAD r]), dims=2)
    didx  = didx[idx];
    lamRAD   = lamRAD[idx];
    theRAD   = theRAD[idx];
    r     = r[idx];
    parts = ceil(length(lamRAD)/chunklength);


    for ridx = 1:parts
        idx     = (ridx - 1) .* chunklength .+ 1:min(ridx * chunklength, num_coord);
        idx_len = length(idx);

        # prepare ratio Earth radius over radius
        idx = floor.(Int,idx)
        TF = broadcast(^, a_E ./ r[idx], tl);
        TA = zeros(length(idx), row);
        TB = zeros(length(idx), row);

        # unwrapping to avoid if ... else for order m = 0
        Cnm = field[:, row];# get Cnm coefficients for order 0
        #disp(plm_func(l, 0, theRAD(idx)))
        TFP = TF .* LegendreF(l, 0, theRAD[idx]); # multiply TF with fully normalized Legendre Polynoms
        TA[:, 3] = TFP * Cnm;                    # for m = 0: Snm = 0, hence TB also = 0
        for m = 1:(row - 1) # rest of order m
            Cnm = field[:, row + m];             # get Cnm coefficients for order m
            Snm = field[:, row - m];             # get Snm coefficients for order m
            TFP = TF .* LegendreF(l, m, theRAD[idx]); # multiply TF with fully normalized Legendre Polynoms
            TA[:, m + 1] = TFP * Cnm;
            TB[:, m + 1] = TFP * Snm;
        end

        # now do the final summation
        mlam = lamRAD[idx] * l_trans;         # matrix of size(length(lam), length(m))
        f[didx[idx]] .= sum(TA .* cos.(mlam) + TB .* sin.(mlam));
    end
    return f
end
