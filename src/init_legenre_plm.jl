"""
    init_legenre_1st_kind_plm(lmax,msel,Wmm,Wlm_1,wlm_2)
`init_legenre_1st_kind_plm` gives the preparational computations for Legendre functions in the order: P00, P10, P20, P30, ... P11, P21, P31, ... P22, P32, ...

# Input:
- `lmax::Integer`: Max value of degree possible.
- `msel::Integer`: Order.

# Output:
- `Wmm::Array{Float}`: P00, P10, P20, P30, ...
- `Wmm1::Array{Float}`: P11, P21, P31, ...
- `Wmm2::Array{Float}`: P22, P32, ...

# Example:
```julia-repl
julia> init_legenre_1st_kind_plm(3,1)
([1.7320508075688772, 0.0], [2.23606797749979, 2.091650066335189, 0.0], [0.9354143466934853, 0.0])
```
"""
function init_legenre_1st_kind_plm(lmax,msel)
    Wmm    = zeros(msel + 1);
    Wlm_1  = zeros(lmax - msel + 1);
    Wlm_2  = zeros(lmax - msel);
    l = 0;m = 0;
    id_mm = 0;id_lm_1 = 0;id_lm_2 = 0;
    for m = 0:msel
        if (m > 0)
            if  (m == 1)
                Wmm[id_mm + 1] = sqrt(3)
                id_mm = id_mm + 1
            else
                Wmm[id_mm + 1] = sqrt((2 *m + 1)/(2*m))
                id_mm = id_mm + 1
            end
        end
        if (msel == m)
            if ((m+1) <= lmax)
                Wlm_1[id_lm_1 + 1] = sqrt(2 *m + 3);
                id_lm_1 = id_lm_1+1
            end
            for l = (m+2):lmax
                # print(1)
                # print(id_lm_1)
                Wlm_1[id_lm_1 + 1] = sqrt((4.0 * l * l - 1)/((l + m)*(l - m)));
                Wlm_2[id_lm_2 + 1] = sqrt(1 - (4 * m * m - 1) / ((l+m)*(l-m)*(2 * l - 3)));
                id_lm_1 = id_lm_1 + 1;
                id_lm_2 = id_lm_2 + 1;
            end
        end
    end
    return (Wmm,Wlm_1,Wlm_2)
end
