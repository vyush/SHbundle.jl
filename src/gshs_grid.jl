"""
    gshs_grid(field,lamRAD,phiRAD,a_E;height = 0,max_lm = Inf,quant = "potential",sub_grs80 = true,gm = NaN,curvature = false)

`GSHSGRID` calculates a global spherical harmonic synthesis for any grid
 defined by lam and phi (both vectors). The radius must be scalar.

 f = gshs_grid(field, lamRAD, phiRAD, a_E)

 # Input:
    - `field::Array{Float}`: gravity field in |c\\s| or /s|c\\ format
    - `lamRAD::Array{Float}` :longitude [rad]
    - `phiRAD::Array{Float}`: latitude  [rad]
    - `a_E::Float`: semi major axis of the Earth rotational ellipsoid
    - `height::Float`:(default: 0), height [m]
    - `max_lm::Integer`: maximum degree/order (default: determined from field)

    - `quant::String`: defining the field quantity:
                  - 'potential' ... (default), potential [m^2/s^2], needs 'GM'
                  - 'tr' .......... gravity disturbance [mGal], needs 'GM'
                  - 'trr' ......... 2nd rad. derivative [E], needs 'GM'
                  - 'none' ........ coefficients define the output
                  - 'geoid' ....... geoid height [m]
                  - 'dg', 'gravity' gravity anomaly [mGal], needs 'GM'
                  - 'slope' ....... slope [arcsec]
                  - 'water' ....... equivalent water thickness [m]
                  - 'smd' ......... surface mass density
    - `sub_grs80::Boolean`: (default: true) determines whether WGS84 is subtracted.
    - `GM::Float`: geocentric gravitational constant GM
    -`curvature::Boolean` : (Default: false) consider curvature of the chosen quantity.

# Output:
    - `f::Array{Float}` :field quantity


"""
function gshs_grid(field,lamRAD,phiRAD,a_E;height = 0,max_lm = Inf,quant = "potential",sub_grs80 = true,gm = NaN,curvature = false)
    (row,col) = size(field)
     if (col == row)
        firld = cs2sc(field);
    elseif (col != 2*row - 1)
        error("Input field not in cs or sc format");
    end

    if (max_lm > row -1)
        max_lm = row - 1;
    elseif (max_lm < (row - 1))
        field = field[1:max_lm +  1 , (row - max_lm):(row + max_lm)];
        row = size(field ,1);
    end

    if (sub_grs80)
        field = field - cs2sc(normalklm(max_lm, typ = "grs80"));
        @warn("A reference field (grs80) is removed from your coefficients")
    end
    lamRAD = reshape(lamRAD,(length(lamRAD),1));
    phiRAD = reshape(phiRAD,(length(lamRAD),1));
    theRAD= (pi/2 .- phiRAD);
    if (!(isa(height, Union{Number,AbstractString,Char,Bool})))
        error("height must be scalar")
    end
    m       = (0:max_lm);
    l       = m;
    m = m'
    mlam    = (lamRAD * m)';      #'
    len_the = length(theRAD);
    transf  = eigengrav(l, fstr = quant, h = height, constant = [gm, a_E]);
    field   = diagm(transf[:]) * field;

    if (curvature)
        TAp = zeros(len_the, row);
        TBp = zeros(len_the, row);
        TAl = zeros(len_the, row);
        TBl = zeros(len_the, row);
        TApp = zeros(len_the, row);
        TBpp = zeros(len_the, row);
        TAll = zeros(len_the, row);
        TBll = zeros(len_the, row);
        TApl = zeros(len_the, row);
        TBpl = zeros(len_the, row);

         # unwrapping to avoid if ... else for order m = 0 (i.e. Snm = 0)
        Cnm = field[:, row];              # get Cnm coefficients for order 0
        (P ,dP, ddP) = LegendreF(l, 0, theRAD,nret = 3); # calc fully normalized Legendre Polynoms
        TAp[:, 1]  = -dP * Cnm; # all variables multiplied with Snm are 0, of course
        TBl[:, 1]  =  -P * Cnm;
        TApp[:, 1] = ddP * Cnm;
#          print(TApp[:,1])        TAll[:, 1] =  -P * Cnm;
        TBpl[:, 1] =  dP * Cnm;
        @showprogress 1 "Computing..." for m = 1:(row - 1)
            Cnm = field[:, row + m];          # get Cnm coefficients for order m
            Snm = field[:, row - m];          # get Snm coefficients for order m
            (P ,dP, ddP) = LegendreF(l, m, theRAD,nret = 3); # calc fully normalized Legendre Polynoms
            TAp[:, m+1]  = -dP * Cnm;  TBp[:, m+1]  = -dP * Snm;
            TAl[:, m+1]  =   P * Snm;  TBl[:, m+1]  =  -P * Cnm;
            TApp[:, m+1] = ddP * Cnm;  TBpp[:, m+1] = ddP * Snm;
            TAll[:, m+1] =  -P * Cnm;  TBll[:, m+1] =  -P * Snm;
            TApl[:, m+1] = -dP * Snm;  TBpl[:, m+1] =  dP * Cnm;
        end
        cosmlam = cos.(mlam);
        sinmlam = sin.(mlam);
        fp  = TAp  * cosmlam + TBp  * sinmlam;
#          display(fp)        fp2 = fp.^2;
#        print(fp2[:,1])        fl  = TAl  * cosmlam + TBl  * sinmlam;
#           display(fl)        fl2 = fl.^2;
#          print(fl2)
        fpp = TApp * cosmlam + TBpp * sinmlam;
#          display(fpp)
        fll = TAll * cosmlam + TBll * sinmlam;
#         display(fll)
        fpl = TApl * cosmlam + TBpl * sinmlam;
#         display(fpl)
        f = ((1 .+ fp2) .* fll - 2 .* fp .* fl .* fpl + (1 .+ fl2) .* fpp) ./ (2 .* (sqrt.(1 .+ fp2 + fl2)).^3);
    else
         TA = zeros(len_the, row);
         TB = zeros(len_the, row);

        Cnm = field[:, row];                # get Cnm coefficients for order 0
        TA[:, 1] = LegendreF(l, 0, theRAD) * Cnm; # for m = 0: Snm = 0, hence TB also = 0
        for m = 1:row-1
            Cnm = field[:, row + m];     # get Cnm coefficients for order m
            Snm = field[:, row - m];     # get Snm coefficients for order m
            P = LegendreF(l, m, theRAD);  # calc fully normalized Legendre Polynoms
            # ------------------------------------------------------------------
            TA[:, m + 1] = P * Cnm;
            TB[:, m + 1] = P * Snm;
        end

        # now do the final summation
        f = TA * cos.(mlam) + TB * sin.(mlam);

    end
    return f;
#     return f;
end
