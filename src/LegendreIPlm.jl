using SHbundle
#     X = [i for i in x, j in 1:length(y)]
#     Y = [j for i in 1:length(x), j in y]
#     return X, Y
# end
"""
    LegendreIplm(l,m,theRAD;dt = Inf)

IPLM Integrals of the fully normalized associated Legendre functions over blocks for a selected order M.
# Input
- `l::Array{Integer}`: Degree.For l < m a vector of zeros will be returned.
- `m::Integer`: Order.
- `thetaRAD::Array{Float}`: Co-Latitude in radians.
- `dt::Float`:integration block-size in radians. (Default = theRAD(2)-theRAD(1))
# Output
- `p::Array{Float}`: Matrix with integrated Legendre functions.Functions are integrated from theRAD(i)-dt/2 till theRAD(i)+dt/2.The matrix has length(TH) rows and length(L) columns, unless L or TH is scalar. Then the output vector follows the shape of respectively L or TH.

# Examples
```julia-repl
julia> LegendreIplm((0:4), 0, (0:0.1:0.5))
6×5 Matrix{Float64}:
 0.0         0.0        0.0        0.0        0.0
 0.00997918  0.0171766  0.021898   0.0254234  0.0281012
 0.0198587   0.0336684  0.0416187  0.0460588  0.0476029
 0.0295397   0.048818   0.0571813  0.0579053  0.0521557
 0.0389256   0.0620214  0.0669811  0.0584098  0.0393473
 0.0479226   0.0727521  0.0699344  0.0468976  0.0114004
```
# Uses
- `Functions` : LegendreF.
"""

function LegendreIplm(l,m,theRAD;dt = Inf)
    # println(1)
    if (length(theRAD) == 1)
        dt = pi/180;
    else
        dt = theRAD[2] - theRAD[1];
    end
    if (length(l) != maximum(size(l)))
        error("Degree L mus be vector or scalar");
    end
    if (any(l .- floor.(l) .!= 0))
        error("Vector l contains non-integers.")
    end
    if(length(m)!=1)
        error("Order M must be scalar");
    end
    if(rem(m,1)!=0)
        error("Order M must be integer")
    end
    if(length(dt)!=1)
        error("Block size DT must be scalar");
    end
    if(dt == 0)
        error("Block size DT cannot be zero")
    end
    # println(1)
    # print(size(l))
    # l = reshape(l,(1,length(l)));
    # theRAD = reshape(theRAD,(1,length(theRAD)));
    lcol = length(l);
    trow = size(theRAD)[1];
    n = length(theRAD);
    theRAD = theRAD[:];
    # print(size(theRAD))
    # println(size(l))
    lmax = maximum(l);
    mfix = m;
    lvec = l[:]';
    l = mfix:lmax;

    stplus  = sin.(theRAD.+ dt/2);
    stmin   = sin.(theRAD.- dt/2);
    ctplus  = cos.(theRAD.+ dt/2);
    ctmin   = cos.(theRAD.- dt/2);
    plmplus = ones(n,lmax+1);
    plmmin  = ones(n,lmax+1);
    plmplus[:,l.+1] = LegendreF(l,mfix,(theRAD.+dt/2));	# tesserals
    plmmin[:,l.+1]  = LegendreF(l,mfix,(theRAD.-dt/2));
    if mfix > 0
        m   = (1:mfix)';
        # print(size(m))
        mm  = 2 .*m;
        # print(cumprod((mm.+1) ./mm,dims = 2))
        fac = sqrt.(2 .* cumprod((mm.+1) ./mm,dims = 2));

        # print(fac)
        # print(size(m))
        # print(size(ones(length(stplus))))
        # mgr = m' .* ones(length(stplus));
        # stp = ones(length(m))' .* stplus;
        # print(size(stplus))
        # (mgr,stp) = meshgrid(m,stplus);
        mgr = m .* ones(size(stplus))
        stp = stplus .* ones(size(m))
        # print(size(stp))
        fgr = fac .* ones(size(stmin))
        stm = stmin .* ones(size(fac))
        # mgr = mgr';
        # stp = stp';
        # print(stp)
        # fgr = fac' .* ones(length(stmin))
        # stm = ones(length(fac))' .* stmin;
        # (fgr,stm) = meshgrid(fac,stmin)


        # fgr = fgr';
        # stm = stm';
        # print(size(fgr))
        # print(size(stm))
        plmplus[:,m.+1]  = (fgr.*(stp.^mgr));
        # print(plmplus)		# sectorials
        plmmin[:,m.+1]   = (fgr.*(stm.^mgr));
        # print(plmmin)
    end
    # Initialization of the temporary matrix ptmp, containing the integrated
    # functions in its columns, with progressing degree l. The last column of
    # ptmp will contain zero|s, which is useful for assignments when l < m.
    ptmp   = zeros(n,lmax+2);				# L+2 columns !!!
    ptmp00 = cos.(theRAD .- dt/2) .- ctplus;
    ptmp11 = sqrt(3) ./ 2 .* (dt .- ctplus.*stplus .+ ctmin.*stmin);
    ptmp10 = sqrt(3) ./ 2 .* (stplus.^2 .- stmin.^2);
    ptmp[:,1] = ptmp00;


    # Compute first the integrals of order m == 0
    if (mfix == 0)

        ptmp[:,2] = ptmp10;
        for l = 2:lmax					# loop over the degree l
            rootnm  = sqrt( (2 .*l.+1) * (2 .*l .-1) / l.^2 );
            root1nm = sqrt( (2 .*l.-1) * (2 .*l.-3) / (l.-1).^2 );
            ptmp[:,l.+1] = rootnm / (l.+1) * ( (l.-2) * ptmp[:,l.-1] / root1nm + stplus.^2 .* plmplus[:,l] - stmin.^2 .* plmmin[:,l] );
        end
    else
        # Compute the integrals of order m > 0

        # First we compute the diagonal element IPmm (lmax == mfix)
        ptmp[:,2] = ptmp11;
        for l = 2:mfix					# loop over the degree l
            rootmm  = sqrt.( (2 .*l.+1) ./ (2 .*l) );
            root1mm = sqrt.( (2 .*l.-1) ./ (2 .*l.-2));
            if l == 2
                root1mm = sqrt(3);
            end
            ptmp[:,l+1] = rootmm / (l+1) * ( l * root1mm * ptmp[:,l-1] - ( ctplus .* plmplus[:,l+1] - ctmin .* plmmin[:,l+1] ) / rootmm );
            if(l==2)
                # print(  plmplus[:,l+1])# - ctmin .* plmmin[:,l+1] ) );
                # rootmm / (l+1) * ( l * root1mm * ptmp[:,l-1] - ( ctplus .* plmplus[:,l+1] - ctmin .* plmmin[:,l+1] ) / rootmm )
            end
        end

        # print(size(ptmp))
        # the arbitrary element IPlm ( computed only when lmax > mfix)

        if (lmax > mfix)

        # first we do the element IPlm, for which l - m = 1
            l = mfix + 1;
            rootnm = sqrt( (2*l+1) * (2*l-1) / (l+mfix) / (l-mfix) );
            ptmp[:,l+1] = rootnm / (l+1) * (stplus.^2 .* plmplus[:,l] - stmin.^2 .* plmmin[:,l] );

        # now we do the rest
            for l = mfix+2:lmax                   # loop over the degree l
                rootnm  = sqrt( (2*l+1) * (2*l-1) / (l+mfix) / (l-mfix) );
                root1nm = sqrt( (2*l-1) * (2*l-3) / (l-1+mfix) / (l-1-mfix) );
                ptmp[:,l+1] = rootnm / (l+1) * ( (l-2) * ptmp[:,l-1] / root1nm + stplus.^2 .* plmplus[:,l] - stmin.^2 .* plmmin[:,l] );
            end
        end

    end
    # print(ptmp)
    # print(mfix)
    lind  = findall(lvec->lvec < mfix,lvec);			# index into l < m
    # print(lind)
    pcol  = lvec .+ 1;				    # index into columns of ptmp
    pcol[lind] = (lmax+2) * ones(size(lind));	# Now l < m points to last col.
    pcol = pcol[:];
    p     = ptmp[:, pcol];	    # proper column extraction
    # print(size(p))
    if ((maximum(size(lvec)) == 1)   && (minimum(size(theRAD)) == 1) && (trow == 1))
        p = p';
    end
    if maximum(size(theRAD)) == 1 && minimum(size(lvec)) == 1   && (lcol == 1)
        p = p';
    end
    return p
end
