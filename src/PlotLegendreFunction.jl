using SHbundle
"""
    PlotLF(l, m, theta)

PlotLF plots the Legendre function for a particular degree and order for a range of co-latitudes.
# Input
- `l::Array{Integer}`: Degree.
- `m::Integer`: Order.
- `theta::Array{Float}`: Co-Latitude in radians.
# Output
-  `Fig::Plot`:The 2-D plot of P_lm vs Co-Latitude .

# Examples
```julia-repl
julia> PlotLF(20,0,[0:0.01*pi/180:pi;])
Plot{Plots.GRBackend() n=1}
```
# Uses
- `Package` : Plots.
- `Functions` : plot,xlabel,ylabel,display,LegendreF.
"""
function PlotLF(l,m,theta)
    y = LegendreF(l,m,theta)
    theta1 = theta .*(180/pi)
    fig = plot(theta1,y;title = "Legendre Function",lw = 3)
    xlabel!("θ")
    ylabel!("P_lm")
    display(fig);
end
