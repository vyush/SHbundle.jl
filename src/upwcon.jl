using SHbundle

"""
    upwcon(degree;height =0 ,constant = [])
`UPWCON` returns the upward continuation (R/r)^l.

# Input
- `degree::Integer`: Spherical harmonic degree.
- `height::Float`: Height above mean Earth radius in meters(Default: 0).
- `const::Array{Float}`: Reference ellipsoid constants: `[GM a_E]`(`Default: [3.986005e14 6378137]`).
# Output
- `uc::Float`: Upward continuation terms.
# Examples
```julia-repl
julia> upwcon(240,height = 10)
0.9996237856550613

julia> upwcon(24)
1.0
```
# Uses
- `Functions`: standing,lying.
"""
function upwcon(degree;height = 0 ,constant = [])
    if (isempty(constant))
        ae     = 6378137;
        GM     = 3.986005e14;
    elseif (length(constant) == 2)
        GM = constant[1];
        ae = constant[2];
    else
        error("Please verify the input constants.");
    end
    if length(height) > 1
       degree = lying([degree]);
       height = standing([height]);
       rr = ae.*ones(size(height)) ./ (ae.+height);
       uc = (rr .* ones(size(degree))) .^ (ones(size(height)) .* degree);
    else
       rr = ae ./ (ae.+height);
       uc = rr.^degree;
    end
    return uc;
end
