using SparseArrays
"""
    normalklm(lmax;tup = "grs80")
Normalklm returns an ellipsoidal normal field consisting of normalized - Jn, n=0,2,4,6,8
# Input
- `lmax::Integer`: Maximum Degree.
- `typ::String`: Either `"wgs84"`,`"grs80 (default)"` or `"he (or hydro)"`
# Output
-  `nklm::SparseMatrix{Float,Integer}`: Normal field in CS-format (sparse) .

# Examples
```julia-repl
julia> normalklm(2)
3×3 SparseMatrixCSC{Float64,Int64} with 2 stored entries:
  [1, 1]  =  1.0
  [3, 1]  =  -0.000484167

julia> normalklm(2,typ = "he")
3×3 SparseMatrixCSC{Float64,Int64} with 2 stored entries:
  [1, 1]  =  1.0
  [3, 1]  =  -0.000479689
```
# Uses
- `Package` : SparseArrays.
- `Functions` : sparse,ones.
"""
function normalklm(lmax;typ = "grs80")

        if (!isa(lmax, Number))
            error("LMAX should be scalar.");
        end

        if(lmax<0)
            error("LMAX should be positive.");
        end
        if (floor(lmax) != lmax)
            error("LMAX should be integer.")
        end

        if(lowercase(typ) == "wgs84")
            J2     =  1.08262982131e-3;     # earth's dyn. form factor (= -C20 unnormalized)
            J4     = -2.37091120053e-6;     # -C40 unnormalized
            J6     =  6.08346498882e-9;     # -C60 unnormalized
            J8     = -1.42681087920e-11;    # -C80 unnormalized
            jcoefs = [1,-J2,-J4,-J6,-J8];
            l      = [0:2:min(lmax,8);]

        elseif(lowercase(typ) == "grs80")
            J2     =  1.08263e-3;           # earth's dyn. form factor (= -C20 unnormalized)
            J4     = -2.37091222e-6;        # -C40 unnormalized
            J6     =  6.08347e-9;           # -C60 unnormalized
            J8     = -1.427e-11;            # -C80 unnormalized
            jcoefs = [1,-J2,-J4,-J6,-J8];
            l      = [0:2:min(lmax,8);];

        elseif(lowercase(typ) == "he" || lowercase(typ) == "hydro")
            J2     = 1.072618e-3;           # earth's dyn. form factor (= -C20 unnormalized)
            J4     = 0.2992e-5;             # -C40 unnormalized
            jcoefs = [1,-J2,-J4];
            l      = [0:2:min(lmax,4);];
        else
            error("Unknown type of ellipsoid: " * typ);
        end
         range = [1 : length(l);]
         coefs = jcoefs[range] ./ sqrt.( 2 .* l .+ 1)
         nklm = sparse(l .+ 1, ones(size(l)), coefs , lmax .+ 1, lmax .+ 1)
    return nklm;
end
