using LinearAlgebra;
using FFTW
using SHbundle
#? Some mistake difference in IFFT implementation.
#? will complete doumnetation after that..
"""
    ispec(a,b)
`ispec` returns the function `f` from the spectra `a` and `b`.
# Input
- `a::Array{Float}`: cosine coefficients .
- `b::Array{Float}`: sine coefficients.
a and b are defined by:
                - f(t) = a_0 + SUM_(i=1)^n2 a_i*cos(iwt) + b_i*sin(iwt),with w = ground-frequency and n2 half the number of samples (+1).Note that no factor 2 appears in front of the sum.
# Output
- `F::Array{Float}`:If  A and B are matrices, Fourier operations are columnwise..

# Examples
```julia-repl
julia> ispec2([1],[0])
1×1 Matrix{Float64}:
 2.0
```
"""
function ispec2(a,b)
    # setprecision(5);
    n2 = size(a,1);
    #print(n2);
    a[1,:] = 2 .* a[1,:];
    #print(b[n2,:])
    if(collect(abs.(b[n2,:]) .< 1e-10)[1,1])
        n = 2*n2 - 2;
        a[n2,:] = a[n2,:] .* 2;

        fs = (a .- 1im .* b) ./ 2;

        fs = [fs;conj.(fs[n2 - 1:-1:2,:])] .* max(n,1);
    else
        n = 2*n2 -1;
        fs = (a .- 1im .* b) ./ 2;
        fs = [fs;conj.(fs[n2:-1:2,:])] .* n;
    end

    x = ifft(fs,(1,));
    f = real(ifft(fs,(1,)));
    return f;
end
