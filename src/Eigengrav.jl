"""
    eigengrav(lmax ; fstr = "",h = 0,constant = [])
EIGENGRAV(lmax, fstr, h, type) returns the isotropic spectral transfer (or: eigenvalues) of several gravity related quantities.Upward continuation may be included.
# Input
- `lmax::Integer`: Spherical harmonic degree.
- `fstr::String`:  Denotes the functional under consideration:
    - `none` : Ones.
    - `geoid` :
    - `dg` , `gravity` : Gravity anomaly.
    - `potential` : Potential.
    - `tr` : gravity disturbance.
    - `trr` : (d^2/dr^2).
    - `slope` : size of surface gradient.
    - `water` : equivalent water thickness.
    - `smd` : surface mass density.
- `h::Float`: height above Earth mean radius in meter(Default : 0).
- `const::Array{Float}`: Reference ellipsoid constants: `[GM a_E]`(`Default: [3.986005e14 6378137]`).
# Output
- `tf::Array`: transfer. Size and shape equal to lmax. Units according to `fstr`.
# Examples
```julia-repl
julia> eigengrav(3,fstr = "trr",h = 10)
30724.315694489425

julia> eigengrav([3,2],fstr = "geoid",h = 10)
2-element Array{Float64,1}:
 6.3781170000470355e6
 6.378127000015679e6
```
# Uses
- `Functions`: upwcon.
"""
function eigengrav(lmax ; fstr = "",h = 0,constant = [])
    # general checks
    #     if(fstr == "" &&  h == 0 && length(const) = 0) error("Gimme more")end;
    #     if !(isa(gridsize, Union{Number,AbstractString,Char,Bool})),      error("H should be scalar."),  end;
    #     if !isa(fstr,Char)     error("FSTR must be string.") end;
    #     if !isa.(lmax)), error('L must be integer.'),   end;
    #     if ~isvector(lmax),   error('L must be vector.'),    end;
    #     if min(lmax) < 0,     error('Negative L occurs.'),   end;
        fstr = lowercase(rstrip(fstr));

        if(isempty(constant))
            ae     = 6378137;
            GM     = 3.986005e14;
            constant = [GM ae];
        elseif length(constant) == 2
            GM = constant[1];
            ae = constant[2];
        else
            error("Please verify the input constants.");
        end

        r = ae + h;

            if (fstr == "none")
                tf = ones(size(lmax));
            elseif (fstr == "geoid")
                tf = ones(size(lmax)) .* r;                  # [m]
            elseif ( fstr == "potential")
                tf = ones(size(lmax)) .* GM./r;		    	# [m^2/s^2]
            elseif (fstr == "gravity"||fstr ==  "dg")
                tf = (lmax.-1) .* GM./r./r .* 1e5;		    	# [mGal]
            elseif (fstr =="tr")
                tf = -(lmax.+1) * GM./r./r .* 1e5;		     	# [mGal]
            elseif (fstr =="trr")
                tf = (lmax.+1) .* (lmax .+2) .* GM./r./r./r .* 1e9;	# [E]
            elseif (fstr =="slope")
                tf = sqrt.(lmax .* (lmax .+ 1));			    # [rad]
            elseif (fstr =="water")
                tf = (2 .*lmax.+1) ./ (1 .+ lovenr.(lmax)) .* r .* 5.517 ./ 3;  # [m]
            elseif (fstr =="smd")
                tf = (2 .*lmax.+1) ./ (1 .+ lovenr.(lmax)).* r .* 5517 ./ 3;   # [kg/m^2]
            else
                error("Requested functional FSTR not available.")
        end
        if (h>0)
            tf = upwcon(lmax,height = h,constant = constant) .* tf;
        end
        return tf;
    end
