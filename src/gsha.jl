using ProgressMeter
using SHbundle
"""
gsha(f, method; grd = "def", lmax = Inf)

GSHA global spherical harmonic analysis.
# Input:
- `f::Array{Float}`:global field of size (lmax+1)*2*lmax or lmax*2*lmax.
- `method::String`:defining the analysis method:
    - `ls`:least squares.
    - `wls`: weighted least squares
    - `aq`:  approximate quadrature
    - `fnm`: first neumann method
    - `snm`: second neumann method
    - `mean`: block mean values (use of integrated Plm)
- `grd`: defining the grid:
    - `pole`, `mesh`: (default if lmax+1), equi-angular (lmax+1)*2*lmax,
                            including poles and Greenwich meridian.
    - `block`, `cell` ..... (default if lmax), equi-angular block midpoints lmax*2lmax
    - `neumann`, `gauss` .. Gauss-Neumann grid (lmax+1)*2*lmax
- `lmax`:maximum degree of development.

# Output:
- `cs`: Clm, Slm in |C\\S| format.
# Uses:
- `Package`: ProgressMeter
- `Function`: LegendreF,LegendreIplm,neumann,sc2cs.
"""

function gsha(f, method; grd = "def", lmax = Inf)
    #--------------------------------------------------------------------------
    # Grid definition
    #--------------------------------------------------------------------------
    (rows, cols) = size(f);

    if (cols == 2 * rows)                     # 'block' | 'cell' grid
       if (lmax==Inf)
            lmax = rows;
       end      # default
       if (grd == "def")
            grd = "block";
        end
       if ((grd != "block") && (grd != "cell"))
          error("Your GRID variable should be either block or cell")
       end

       n     = rows;
       dt    = 180 / n;
       theta = (dt/2:dt:180);
       lam   = (dt/2:dt:360)';               # dt = dlam

    elseif (cols == 2 * rows - 2)             # 'pole' | 'mesh' | 'neumann'
       if (lmax==Inf)
            lmax = rows-1;
       end      # default
       if (grd == "def")
            grd = "pole";
        end    # default

       n     = rows - 1;
       dt    = 180 / n;
       if ((grd == "pole") || (grd == "mesh"))
          theta = (0:dt:180);
          lam   = (0:dt:360-dt)';
       elseif ((grd == "neumann") ||(grd == "gauss"))
          (gw,gx) = neumann(n+1);
          theta = acos.(reverse(gx, dims = 1)).*180 ./pi;
          lam   = (0:dt:360-dt)';
       else
          error("The wrong type of GRID")
       end

    else
        display(size(f));
       error("Invalid size of matrix F")
    end

    theRAD = theta .* pi./180;
    if ((minimum(theRAD) < 0) || (maximum(theRAD)>pi))
        @warn("Is the co-latitude ''thetaRAD'' given in radian?")
    end

    #--------------------------------------------------------------------------
    # further diagnostics
    #--------------------------------------------------------------------------
    if !(isa(grd,String) && isa(method,String)) # deprecated: ~isstr(grd) || ~isstr(method)
        error("GRID and METHOD must be strings")
    end
    if ((method ==  "snm") && !((grd == "neumann") || (grd == "gauss")))
        error("2nd Neumann method ONLY on a neumann/gauss GRID")
    end
    if ((method == "mean") &&!((grd ==  "block") || (grd == "cell")))
        error("Block mean method ONLY on a block/cell GRID")
    end
    if (lmax > n)
        error("Maximum degree of development is higher than number of rows of input.")
    end


    #--------------------------------------------------------------------------
    # Init.
    #--------------------------------------------------------------------------
    L   = n;
    # a   = zeros(rows, L+1);
    # b   = zeros(rows, L+1);
    clm = zeros(L+1, L+1);
    slm = zeros(L+1, L+1);

    #--------------------------------------------------------------------------
    # 1st step analysis: Am(theta) & Bm(theta)
    #--------------------------------------------------------------------------
    m = [0:L;]';
    c = cos.(lam' * m .* pi./180);
    s = sin.(lam' .* m .* pi./180);

    # preserving the orthogonality (except for 'mean' case)
    # we distinguish between 'block' and 'pole' type grids (in lambda)

    if ((grd == "block") || (grd == "cell"))
        if (method ==  "mean")
            dl = dt;                    # longitude block size
            c[:, 1] = dl./360 .* ones(2 * n, 1);	# ICm for m = 0, including 1/(1+dm0)/pi
            m       = 1:L';
            ms      = 2 ./ m .* sin.(m .* dl./2 .* pi./180) ./ pi;
            c[:, 2:L+1] = c[:,2:L+1] .* ms[ones(2*n,1),:];	# ICm
            s[:, 2:L+1] = s[:,2:L+1] .* ms[ones(2*n,1),:];	# ISm
        else
            c = c/L; s = s/L;
            c[:, 1]   = c[:,1]./2;       # / (1 + dm0 - dmL)
            s[:, L+1] = s[:,L+1]./2;     # / (1 - dm0 + dmL)
            c[:, L+1] = zeros(2*n, 1);	# CLL unestimable
            s[:, 1]   = zeros(2*n, 1);	# Sl0    -"-
        end
    else                                # 'pole' | 'mesh' | 'neumann'
        c = c/L; s = s/L;
        c[:,[1 L+1]] = c[:,[1 L+1]]./2;	# / (1 + dm0 + dmL)
        s[:,[1 L+1]] = zeros(2*n,2);	# / (1 - dm0 - dmL), Sl0 & SLL unestimable
    end

    a = f * c;
    b = f * s;

    if(lowercase(method) == "ls")#The linear equation solution varries by very small value.
        @showprogress 1 "Computing..." for m = 0:L
            p = LegendreF(m:L,m,theRAD);
            ai = a[:,m+1];
            bi = b[:,m+1];
            # print(p\ai);
            clm[m+1:L+1, m+1] = p \ ai;
            slm[m+1:L+1, m+1] = p \ bi;
        end
    elseif(lowercase(method) == "wls")#The linear equation solution varries by very small value.
        si = sin.(theRAD);
        si = 2 .* si / sum(si);
        @showprogress 1 "Computing..." for m = 0:L
            p = LegendreF(m:L,m,theRAD);
            ai = a[:,m+1];
            bi = b[:,m+1];
            d = 1 : length(theRAD);
            pts = p' * sparse(d,d,si);
            clm[m+1:L+1, m+1] = (pts * p) \ pts * ai;
            slm[m+1:L+1, m+1] = (pts *p) \ pts * bi;
        end
    elseif(lowercase(method) == "aq")#verified
        si = sin.(theRAD);
        si = 2 .* si / sum(si);
        @showprogress 1 "Computing..." for m = 0:L
            p = LegendreF(m:L,m,theRAD);
            ai = a[:,m+1];
            bi = b[:,m+1];
            d = 1 : length(theRAD);
            pts = p' * sparse(d,d,si);
            clm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * (si.*ai);
            slm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * (si.*bi);
        end
    elseif(lowercase(method) == "fnm")#verified.
        w = neumann(cos.(theRAD))[1];
        w = w'
        @showprogress 1 "Computing..." for m = 0:L
            p  = LegendreF(m:L, m, theRAD);
            ai = a[:, m+1];
            bi = b[:, m+1];
            clm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * (w.*ai);
            slm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * (w.*bi);
        end
    elseif(lowercase(method) == "snm")#verified
        @showprogress 1 "Computing..." for m = 0:L
            p  = LegendreF(m:L, m, theRAD);
            ai = a[:, m+1];
            bi = b[:, m+1];
            clm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * (gw.*ai);
            slm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * (gw.*bi);
        end
    elseif(lowercase(method) == "mean")#need to verify.
        @showprogress 1 "Computing..." for m = 0:L
            #What is this.
            p  = iplm(m:L, m, theRAD); # integrated Legendre
            ai = a[:, m+1];
            bi = b[:, m+1];
            clm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * ai;
            slm[m+1:L+1, m+1] = (1 + (m==0))/4 .* p' * bi;
        end
    else
        error("Choose a valid METHOD")
    end
    slm = reverse(slm, dims = 2);
    cs  = sc2cs([slm[:, 1:L] clm]);
    cs  = cs[1:lmax+1, 1:lmax+1];
#     print(cs)
    return cs
end
