using SHbundle

"""
neumann(in)
`NEUMANN` returns the weights and nodes for Neumann's numerical integration

 NB1 In 1st N-method, length(x) should not become too high,
 since a linear system of this size is solved. Eg: 500.
 NB2 No use is made of potential symmetries of nodes.

 # HOW:   w     = neumann(x)   -- 1st Neumann method
        [w,x] = neumann(n)   -- 2nd Neumann method (Gauss quadrature)

 # Input:
    - `in::Array{Float}`: number of weights and number of base points

 # Output:
    - `w::Array{Float}`:  quadrature weights
    - `x`:  base points (nodes) in the interval [-1;1]

"""
function neumann(in)
    if (length(in) == 1)   # 2nd Neumann method
        if (rem(in,1) != 0)
            error("integer input argument required.");
        end
        (x,w) = grule(in);
        x = x[:]; w = w[:];   # put vectors upright

    elseif (min(size(in)[1],size(in,2)) == 1)   # 1st Neumann method
        x  = in[:];
        theRAD = acos.(x);   # [rad]
        l  = 0:(length(x)-1);
        pp = LegendreF(l,0,theRAD)';
#         display(size(pp))# normalized Legendre polynomials
        r  = [2;zeros(length(x)-1,1)];  # right-handside vector
#         display(size(r))
        w = pp\r;# solve system of equations
        if (size(x) != size(w))
            w = w';
        end
    else
        error("input argument should be scalar or vector")
    end
    return (w,x)
end
