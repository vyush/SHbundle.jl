using LinearAlgebra
"""
    sc2cs(field)
`sc2cs(field)` converts the rectangular (L+1) x (2L+1) matrix `field`, containing spherical harmonics coefficients in /S|C\\ storage format into a square (L+1) x (L+1) matrix in |C\\S| format.

# Input:
- `field::Array{Float,2}`: The rectangular (L+1) x (2L+1) matrix `field`, containing spherical harmonics coefficients in /S|C\\ storage format

# Output:
- `cs::Array{Float,2}`: square (L+1) x (L+1) matrix in |C\\S| format.
#Examples
```julia-repl
julia> sc2cs([0 0 1 0 0;0 2 3 4 0;5 3 6 7 8])
3×3 Array{Float64,2}:
 1.0  2.0  3.0
 3.0  4.0  5.0
 6.0  7.0  8.0
```
#Uses
- `Package`: LinearAlgebra.
- `Function`: tril,triu,rotl90.
"""
function sc2cs(field)
    (row,col) = size(field);
    field = copy(field);
    if ((row != col) && (col != (2*row-1)))
        error("Input neither in cs nor in sc format");
     elseif (col == row)
         cs = field;
     else
         lmax = row-1;
         c    = field[:,lmax+1:2*lmax+1];
         s    = [zeros(lmax+1,1)  field[:,1:lmax]];
         x    = triu(rotl90(s),1);
        cs   = tril(c) + x;
    end
    return cs;
end
