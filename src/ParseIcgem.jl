using DelimitedFiles
using ProgressMeter
using SHbundle
"""
    ind2subv(shape, indices)
`sun2indv` maps linear indices to cartesian.
# Input
- `shape::Tuple`: d-tuple with size of each dimension.
- `indices::Array`: n-iterable with linear indices.
# Output
- `Output::Array{Tuple}`: n-vector of d-tuples with cartesian indices.
# Examples
```julia-repl
julia> ind2subv( (3,4,2), [3,5])
2-element Array{Tuple{Int64,Int64,Int64},1}:
 (3, 1, 1)
 (2, 2, 1)
```
#Uses
- `Packages`: Interpolations,Dierckx
- `Functions`: Tuple,CartesianIndices.
"""
ind2subv(shape, indices) = Tuple.(CartesianIndices(shape)[indices]);


"""
    sub2indv(shape, indices)
`sun2indv` returns linear indices given vector of cartesian indices.
# Input
- `shape::Tuple`: d-tuple of dimensions.
- `indices::Array{Tuple}`: n-iterable of d-tuples, each containing cartesian indices.
# Output
- `Output::Array`: Vector of linear indices.
# Examples
```julia-repl
julia> sub2indv( (3,4,2), [(3,1,1), (1,2,1)])
2-element Array{Int64,1}:
 3
 4
```
#Uses
- `Packages`: Interpolations,Dierckx
- `Functions`: LinearIndices,CartesianIndex.
"""
sub2indv(shape, indices) = LinearIndices(shape)[CartesianIndex.(indices)]



"""
    parse_icgem(filename; max_lm = Inf, min_lm = 0,verbose = false)
`parse_icgem` reads files in the ICGEM format.
# Input
- `filename::File`: Coefficient path/file name.
- `max_lm::Integer`: Reads coefficients up to a degree = order = max_lm (`Default: Inf`, reads all coefficients).
- `min_lm::Integer`: Omit coefficients below that degree/order(`Default: 0`).
- `verbose::Boolean`: If true, dumps additional info on the screen(`Default: False`).
# Output
- `coeffs::Array{Float}`: "gfc" coefficients from ICGEM file in clm-format.
# Example
```julia-repl
julia> "parse_icgem("/path/to/your/file",max_lm = 2,verbose = false)
parsing file /path/to/your/file ...

Parsing file completed succesfully.
6×6 Array{Float64,2}:
 0.0  0.0   1.0           0.0         0.0         0.0
 1.0  0.0   0.0           0.0         0.0         0.0
 1.0  1.0   0.0           0.0         0.0         0.0
 2.0  0.0  -0.000484169   0.0         1.1666e-10  0.0
 2.0  1.0  -3.10267e-10   1.41065e-9  4.2992e-11  4.2963e-11
 2.0  2.0   2.43937e-6   -1.40029e-6  3.6836e-11  3.6387e-11
```
# Uses
- `Package`: DelimitedFiles,ProgressMeter.
- `function`: open,eof,chomp.
"""
function parse_icgem(filename; max_lm = Inf, min_lm = 0,verbose = false)

    #check parameters
    if (!isfile(filename))
        error("File "* filename *" not found ...");
    end
    println("parsing file " * filename *  " ...\n");
    #First, parse for line identifier and rest of line
     open(filename,"r") do fid

        lines = [0 0] #initalising
        while(! eof(fid))
          tline = chomp(readline(fid))
          if isempty(tline)
              continue;
                end;
            w = 1;
          while (tline[w] == ' ' && w < length(tline)) # blank or tab
                w = w + 1;
          end
          i = w;
          while (tline[i] != ' ' && i < length(tline))
              i = i + 1;
          end
          j = i;
          while (tline[j] == ' ')
              j = j + 1;
              if (length(tline) < j)
                    break;
              end;
          end;
            lines = [lines;[tline[w:i-1] tline[j:end]]]
        end;

     end;
    lines = lines[2:end,:]
    #################################################




    #Second, take line identifiers and parse through rest of line accordingly
    #expection header at first
    header = true;
    err_hand = 1;
    minGO = max_lm;
    maxGO = min_lm;
    #print(max_lm)
#     println((maxGO+1)^2)
    tmp_coeffs = [];

    #println(size(tmp_coeffs))


    j = 0;
    prev = 0;
    @showprogress 1 "Doing the magic..." for i = 1:size(lines,1)
        #sleep(0.1)
           lineid   = uppercase(lines[i,1]);
           linecont = lines[i, 2];


           if lineid == "COMMENT"
                if verbose == true
                    println("NOTICE: "*lineid*" "*linecont);
                end

           elseif lineid == "ERRORS"
                err = rsplit(linecont, " ",keepempty = false);
                if verbose
                    println("NOTICE: "*lineid*" "*err[1,1]); end;
                if cmp(err[1,1], "no") == 0
                    err_hand = 0;
                elseif cmp(err[1,1], "formal") == 0
                    err_hand = 1;
                end

            elseif lineid == "GFC"
                j = j+1;
                if err_hand == 0;
                    lin_tmp = parse.(Float64,rsplit(linecont, " ",keepempty = false));
                    cols = 4;
                    if(j==1)
                        tmp_coeffs = zeros(1,cols);
                    end;
                elseif err_hand == 1
                    lin_tmp = parse.(Float64,rsplit(linecont, " ",keepempty = false));
                    cols = 6;
                    if(j==1)
                        tmp_coeffs = zeros(1,cols);
                    end;
                end

                if (lin_tmp[1] >= min_lm) && (lin_tmp[1] <= max_lm) && (lin_tmp[2] <= max_lm)
                    minGO = min(minGO, lin_tmp[1]);
                    maxGO = max(maxGO, lin_tmp[1], lin_tmp[2]);
                    lin = (lin_tmp[1] + 2) .* (lin_tmp[1] + 1) ./ 2 - (lin_tmp[1] - lin_tmp[2]);
                    lin = floor(Int64,lin)
                    if(lin - prev > 1)
                        # tmp_coeffs[lin, 1:cols] = lin_tmp;
                        #println(cols)
                        tmp_coeffs = [tmp_coeffs;zeros(lin - prev - 1,cols)];
                        lin_tmp = lin_tmp'
                        tmp_coeffs = [tmp_coeffs;lin_tmp];
                        prev = lin;
                    else
                        #println(lin_tmp')
                        lin_tmp = lin_tmp'
                        #println(tmp_coeffs)
                        tmp_coeffs = [tmp_coeffs; lin_tmp];
                        prev = lin;
                    end
                end
            elseif lineid == "PRODUCT_TYPE"
                    if verbose
                        println("NOTICE: "*lineid*" "*linecont);
                    end;
                    #info.product_type = linecont;
            elseif lineid =="MODELNAME"
                    if verbose
                        println("NOTICE: "*lineid*" "*linecont); end;
                    #info.modelname = linecont;
            elseif lineid == "RADIUS"
                    if verbose
                        println("NOTICE: "*lineid*" "*linecont); end;
                    #info.R = parse(Float64,linecont);
            elseif lineid =="EARTH_GRAVITY_CONSTANT"
                    if verbose
                        println("NOTICE: "*lineid*" "*linecont);
                    end;
                    #info.GM = parse(Float64,linecont);
            elseif lineid =="NORM"
                    if verbose
                        println("NOTICE: "*lineid*" "*linecont);end;
                    #info.norm = linecont;
            elseif lineid =="TIDE_SYSTEM"
                    if verbose
                        println("NOTICE: "*lineid*" "*linecont); end;
                    #info.tide_system = linecont;
            elseif lineid == "END_OF_HEAD"
                header = false;
            else
                if !header
                    if verbose
                        println("  NOTICE:  line "* string(i) * " - unknown id: "*lineid*" "*linecont); end;
                end
        end
    end
    size(tmp_coeffs);
    tmp_coeffs = tmp_coeffs[2:end,:];
    coeffs = tmp_coeffs;
    println("Parsing file completed succesfully.");
    return coeffs;
end
