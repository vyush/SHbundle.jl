function standing(a)
    a = copy(a);
    n = size(a)[1];
    m = length(a)/n;
    if (n<m)
        a = a'
    end
    return a;
end
function lying(a)
    n = size(a)[1];
    m = length(a)/n;
    if (n>m)
        a = a';
    end
    return a;
end

"""
    gshs(field;max_lm = Inf , quant = "Potential" , grid = "mesh",gridsize = Inf , height = 0 , sub_grs80 = false)
GSHS global spherical harmonic synthesis.
# Input
- `field::Array{Float}`: set of SH coefficients, either in |c\\s| or /s|c\\ format.
- `max_lm::Integer`: maximum degree/order(`Default: determined from field`).
- `quant::String`: optional string argument, defining the field quantity:
    - `potential(default)`:  (default), potential [m^2/s^2],
    - `tr`: grav. disturbance, 1st rad. derivative [mGal],
    - `trr`: 2nd rad. derivative [E],
    - `none`: coefficients define the output
    - `geoid`: geoid height [m],
    - `dg`, `gravity`: gravity anomaly [mGal],
    - `slope`: size of surface gradient [arcsec],
    - `water`: equiv. water height [m],
    - `smd`: surface mass density [kg/m^2].
- `grid::String` ...... optional string argument, defining the grid:
    - `pole`, `mesh`: (default), equi-angular (n+1)*2n, including poles and Greenwich meridian.
    - `block`, `cell`: equi-angular block midpoints. n*2n.
    - `neumann`, `gauss`: Gauss-grid (n+1)*2n.
- `gridsize::Integer`: grid size parameter n. (Default: n = lmax)
    - longitude samples: 2*n
    - latitude samples n (`blocks`) or n+1.
- `height::Float`: Height above Earth mean radius in meter(`Default: 0`).
- `sub_grs80::Boolean`: if set, subtracts reference ellipsoid WGS84 (`Default: True`).
# Output
- `f::Array{Float}`: The global field.
# Examples
```julia-repl
julia> gshs([1 2 3;4 5 6])
2×2 adjoint(::Matrix{Float64}) with eltype Float64:
  6.66211e8   6.66211e8
 -4.16231e8  -4.16231e8
examples
```
# Uses
- `Packages`:ProgressMeter
- `Functions`:cs2sc,normalklm,eigengrav,LegendreF,ispec2
"""
function gshs(field;max_lm = Inf , quant = "Potential" , grid = "mesh",gridsize = Inf , height = 0 , sub_grs80 = false)
    (row, col) = size(field);
    if (col == row)
        field = cs2sc(field);
    elseif (col != 2 * row - 1)
       error("Input field not in cs or sc format");
    end
    lmax = max_lm;
    if (lmax > (row -1))
        lmax = row - 1;
    elseif (lmax < (row - 1))
        field = field[1:lmax + 1 , (row - lmax) : (row + lmax)];#adjust field
    end
    #println(size(field));
    if (isinf(gridsize))
        n = lmax;
    elseif (isa(gridsize, Union{Number,AbstractString,Char,Bool}) && isa(gridsize,Integer))
        n  = gridsize;
    else
        error("gridsize must be integer & scalar");
    end
#   -------------------------------------------------------------------------
#   Grid definition.
#   -------------------------------------------------------------------------
    if isa(grid,Char)
        error("grid argument must be string")
    end
    dt = pi/n;
    #print(dt);
    if(grid == "pole" || grid == "mesh")
        theRAD = [0:dt:pi;];
        lamRAD = [0:dt:(2*pi-dt);]';
    elseif(grid == "block" || grid == "cell")
        theRAD = [dt/2:dt:pi;];
        lamRAD = [dt/2:dt:2*pi;]';
    elseif(grid == "neumann" || grid == "gauss")
        theRAD = reverse(acos.(standing(gx)),dims = 1);
        lamRAD = [0:dt:2*pi-dt;]';
    else
        error("Sorry we dont know about that grid")
    end
    nlat = length(theRAD);
    nlon = length(lamRAD);

#  -------------------------------------------------------------------------
#  Preprocessing on the coefficients:
#    - subtract reference field (if params.sub_wgs84 is set)
#     - specific transfer
#     - upward continuation
#  -------------------------------------------------------------------------
    if (sub_grs80)
        field = field - cs2sc(normalklm(lmax));
        warning("A reference field (grs80) is removed from your coefficients")
    end

    l = 0:lmax;
    transf = eigengrav(l,fstr = quant,h = height );
    field  = field .* (transf * ones(1, 2 .* lmax .+ 1));



    dlam   = ceil(Int,lmax / n);
    abcols = dlam * n + 1;
    a = zeros(nlat, abcols);
    b = zeros(nlat, abcols);
    # % -------------------------------------------------------------------------
    # % Treat m = 0 separately
    # % -------------------------------------------------------------------------
    a[:, 1] = LegendreF(0:lmax, 0, theRAD) * field[1:lmax+1, lmax+1];


    @showprogress 1 "Computing..." for m = 1:lmax
        p         = LegendreF(m:lmax, m, theRAD);
        a[:, m+1] = p * field[m+1:lmax+1, lmax+1+m];
        b[:, m+1] = p * field[m+1:lmax+1, lmax+1-m];
    end
#     print(a)
    if(grid ==  "block" || grid == "cell")
        m      = [0:abcols-1;];
        m = m';
        cshift = ones(nlat, 1) .* cos(m.*pi ./ 2 ./ n);
        sshift = ones(nlat, 1) .* sin(m.*pi ./ 2 ./n);
        atemp  =  cshift .* a .+ sshift .* b;
        b      = -sshift .* a .+ cshift .* b;
        a      = atemp;
    end
    #print(b)
    if (rem(n,lmax) == 0)
       b[:, abcols] = zeros(nlat, 1);
    end
    #println(size(a));
    # print(a);
    # display(b');
     f = ispec2(a', b')';
     if dlam > 1
        f = f[:, 1:dlam:dlam*nlon];
     end
    #  writedlm( "FileName.csv",  f, ',')
    return f;
end
