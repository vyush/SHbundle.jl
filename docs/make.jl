push!(LOAD_PATH,"..")
using SHbundle
using Documenter

DocMeta.setdocmeta!(SHbundle, :DocTestSetup, :(using SHbundle); recursive=true)

makedocs(;
    modules=[SHbundle],
    authors="Vyush Agarwal",
    repo="https://gitlab.com/vyush/SHbundle.jl/blob/{commit}{path}#{line}",
    sitename="SHbundle.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://vyush.gitlab.io/SHbundle.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
deploydocs(
    repo = "gitlab.com/vyush/Xnumber.jl.git",
)
