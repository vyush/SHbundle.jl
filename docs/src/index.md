```@meta
CurrentModule = SHbundle
DocTestSetup = :(using SHbundle)
```

# SHbundle

Documentation for [SHbundle](https://gitlab.com/vyush/SHbundle.jl).
This package `SHbundle.jl` that contains functions and utilities for spherical harmonics synthesis and analysis.


## Installing Package

You can obtain SHbundle using Julia's Pkg REPL-mode (hitting `]` as the first character of the command prompt):
```julia
(@v1.6) pkg> add "https://gitlab.com/vyush/SHbundle.jl.git"
```
or with
```julia
julia> using Pkg;
julia> Pkg.add(url = "https://gitlab.com/vyush/SHbundle.jl.git")
```
## Importing Package Tools
After installing the package you need to:
```julia
julia> using SHbundle
```

Now you are ready to use the tools provided by package.
```@index
```

```@autodocs
Modules = [SHbundle]
```
